%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author:           Ludovic Delafontaine
% Documentation:    https://gitlab.com/smart-crops/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Document properties
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Type of document
\documentclass[
    english,
    11pt,
    svgnames,
]{report}

% Font encoding
\usepackage[T1]{fontenc}

% Input encoding
\usepackage[utf8]{inputenc}

% Fonts and colors
\usepackage{lmodern}
\usepackage{courier}
\usepackage[table]{xcolor}

% Paper size and margins
\usepackage[
    a4paper,
    width=17cm,
    top=2cm,
    bottom=2cm,
]{geometry}

% Multicols inside document
\usepackage{multirow}
\usepackage{multicol}

%\setlength{\multicolsep}{0pt}

% Tables
\usepackage{tabularx}
\usepackage{tabulary}
\usepackage{booktabs}
\usepackage{csvsimple}

\newcolumntype{R}{>{\raggedleft\arraybackslash}X}

% Rotations
\usepackage{rotating}

% Titles
%\usepackage{titlesec}
%\titlespacing{\section}{0pt}{12pt}{6pt}

% Space between itemize
\usepackage{enumerate}
\usepackage{enumitem}

%\setlist[itemize]{noitemsep}
\renewcommand\labelitemi{--}

% Internationalization
\usepackage[english,main=english]{babel}

% Disable indent for first paragraphes
\setlength{\parindent}{0pt}

% Clickable links and cross-references
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor={black!50!black},
    citecolor={black!50!black},
    urlcolor={black!80!black}
}
\hypersetup{pdfpagemode=UseNone}

% Images
\usepackage{caption}
\usepackage{float}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{tikz}

\graphicspath{{figures/}}

% Debug/testing
\usepackage{blindtext}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Custom packages and commands
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Listings
\usepackage[procnames]{listings}
\usepackage{textcomp}

\definecolor{keywords}{RGB}{255, 0, 90}
\definecolor{comments}{RGB}{0, 0, 113}
\definecolor{red}{RGB}{160, 0, 0}
\definecolor{green}{rgb}{0.21, 0.37, 0.23}
\definecolor{backcolour}{rgb}{0.97, 0.97, 0.97}

\definecolor{pastel-color-1}{HTML}{AD5B54}
\definecolor{pastel-color-2}{HTML}{B8667B}
\definecolor{pastel-color-3}{HTML}{AE7AA3}
\definecolor{pastel-color-4}{HTML}{9094C1}
\definecolor{pastel-color-5}{HTML}{62ADCB}
\definecolor{pastel-color-6}{HTML}{3EC2C0}
\definecolor{pastel-color-7}{HTML}{57D2A3}
\definecolor{pastel-color-8}{HTML}{92DC7F}
\definecolor{pastel-color-9}{HTML}{D4DF64}

\lstdefinestyle{numbers} {
    numbers=left,                   
    numbersep=5pt,
}

\lstset{
    inputpath=scripts,
    language=Python,
    basicstyle=\ttfamily\small,
    keywordstyle=\color{keywords},
    commentstyle=\color{comments},
    stringstyle=\color{red},
    backgroundcolor=\color{backcolour},
    showstringspaces=false,
    identifierstyle=\color{green},
    procnamekeys={def,class},
    upquote=true,
    rangeprefix=""",
    rangesuffix=""",
    includerangemarker=false,
}

% Chemicals
\usepackage[version=4]{mhchem}

% Special forms
\usepackage{pifont}

% Units
\usepackage{siunitx}
\sisetup{
    round-mode=places,
    round-precision=2,
}

% Numbers formatting
\usepackage{numprint}

% Dates
\usepackage[useregional]{datetime2}

% Plots
\usepackage{pgfplots}  
\usepackage{pgfplotstable}  
\usepackage{tikzscale}
 
\pgfplotsset{
    compat=1.15,
}

\usepgfplotslibrary{
    colorbrewer,
    dateplot,
    groupplots,
    statistics,
}
\usetikzlibrary{
    external,
}

\tikzexternalize[prefix=tikz/]
\tikzexternalenable

%\input{plots}

% Comments
\usepackage{verbatim}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TOC, glossaries and bibliography
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Glossories and acronyms
\usepackage[acronym,toc]{glossaries}
\loadglsentries{glossary}
\loadglsentries{acronyms}
\makeglossaries

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Headers and footers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{fancyhdr}
\pagestyle{fancy}

\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancyhead[LE,LO]{}
\fancyhead[CE,CO]{}
\fancyhead[RO,RE]{}

\fancyfoot[LE,LO]{}
\fancyfoot[CE,CO]{}
\fancyfoot[RO,RE]{}

\fancypagestyle{plain}{}

\captionsetup{belowskip=-20pt}

\begin{document}
    \begin{center}
    	\LARGE
        Smart Crops --- Can we model a plant's life from the weather..?
    	
    	\vspace{0.5cm}
    	
        \normalsize
    	\textbf{Ludovic Delafontaine --- Bachelor degree in Software Engineering --- HEIG-VD 2019}
    \end{center}

    \large
    
    \textit{Climate change has completely reshaped our environment for several years. Plants are affected by these changes and if they do not have specific needs to develop and mature, they may die or produce less. Would it be possible to model the plant's life in its environment in order to know in advance what would be its needs using \gls{deep-learning}?}

    \normalsize

    \begin{multicols}{2}
        \section*{Hypothesis}

        Take year-to-year yield data of a certain crop in a given environment. Coupled with the weather data where this given crop grows, see if there is a correlation between crop production and the weather of that environment.

        \section*{Data sources}

        Crops yields data is provided from the farm ``\textit{La ferme pilote de Mapraz}'' in Canton of Geneva. The farm has been collecting data since 2000 and provides production data for many crops. \bigskip

        Weather data is provided by IDAWEB, the Swiss Meteorological Center, from the weather station ``\textit{Genève/Cointrin}''. The data used is from 2000 to 2018 and the measurements are from hour to hour.

        \section*{Creating the model}

        After cleaning, formatting and exploring the data, two crops are selected: \gls{winter-wheat} and \gls{winter-field-beans}. \bigskip

        Four weather variables are used to model the \gls{winter-wheat}'s yield: air temperature, global radiation, precipitation and humidity. Three weather variables are used to model the \gls{winter-field-beans}'s yield: air temperature, global radiation and humidity. \bigskip

        Weather data are resampled and transformed to stationary data. The training and test datasets are created and scaled with normalization or standardization. The model is trained using the \gls{keras} \gls{python} framework. The model's performances are then evaluated using K-Fold cross-validation. The model can then be saved for future predictions. \bigskip

        The \gls{rnn} architecture is a vanilla \gls{gru} layer with a dense layer to predict the yield based on the weather season of the given crop.

        \section*{Results}
        
        Both crops have been successfully modeled with weather data resampled based on weekly mean measurements. \bigskip

        The best model for \gls{winter-wheat} uses 16 neurons, 4500 epochs and data standardization for a mean average percentage error of \SI{9.62}{\percent}. The best model for \gls{winter-field-beans} uses 64 neurons, 3000 epochs and data standardization for a mean average percentage error of \SI{8.74}{\percent}. \bigskip

        For each model, the loss function is \gls{mse} and the optimizer Adam.

        \begin{figure}[H]
            \centering
            \includegraphics[width=0.7\linewidth, height=0.7\linewidth]{plots/winter-field-beans-predictions-five.tikz}
            \caption*{The best \gls{winter-field-beans} model's predictions.}
        \end{figure}

        \vspace{0.1cm}
    
        \section*{Conclusions and use-cases}

        \gls{gru} --- and more generally speaking \gls{deep-learning} --- gives impressive results for regression problems based on time-series. \bigskip

        The scientific rigor and methodology to correctly model the problem to be solved was the most difficult task in this work. But it is the key to effective and unbiased models! \bigskip

        \textbf{Applications}: yields predictions, same crops-kind coupling, resources management.
    \end{multicols}
\end{document}
