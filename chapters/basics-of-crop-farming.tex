A plant has some needs to be able to grow and produce its fruits:

\begin{multicols}{2}
    \begin{itemize}
        \item Water
        
        \item (sun)light
        
        \item Temperature
        
        \item Etc.
    \end{itemize}
\end{multicols}

Those needs can vary during the plant's life and can have a crucial impact on its growth. The environment in which the plant is growing should --- in the best-case scenario --- supply all these needs naturally. \bigskip

With traditional agriculture, farmers shape the environment by providing the plant's needs with water, fertilizer and other external products to make it grow the better. \bigskip

However, with climate change, this environment can be partially --- or completely --- altered and the plant might not have the sufficient needs anymore, even with the intervention of the farmer. The farmer might not have the resources to fill the needs (e.g. insufficient water) or avoid the disasters (e.g. floods) and must adapt to the new environment (see Figure~\ref{fig:field-drought}). \bigskip

\begin{figure}[!htbp]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{introduction/nikita-sypko-95480-unsplash}
        \caption*{\small\textit{Photo by Nikita Sypko} \cite{nikita-sypko}}
    \end{subfigure}
    \caption{Field suffering from the drought.}
    \label{fig:field-drought}
\end{figure}

To understand how farmers produce food, it is necessary to understand how a plant works.

\section{Plant parts}

A plant is composed of six mains parts \cite{what-do-plants-need-to-grow} (see Figure~\ref{fig:plants-parts}):

\begin{itemize}
    \item \textbf{Roots}: allow the plant to adsorb nutriments and water from the soil.
    
    \item \textbf{Stems}: acts as a transport system for the plant.
    
    \item \textbf{Leaves}: transform the light and carbon into food and oxygen using photosynthesis.
    
    \item \textbf{Flowers}: the reproductive part of the plant.
    
    \item \textbf{Fruits}: after a flower has been pollinated and fertilized, it transforms itself into a fruit containing other seeds.
    
    \item \textbf{Seeds}: contain plant material that allow new plants to grow from them.
\end{itemize}

\begin{figure}[!htbp]
    \centering
    \begin{subfigure}[b]{0.65\textwidth}
        \includegraphics[width=\textwidth]{introduction/parts-of-a-plant}
        \caption*{\small\textit{Schema by Rabbits ABC} \cite{rabbits-abc}}
    \end{subfigure}
    \caption{The different parts composing a plant.}
    \label{fig:plants-parts}
\end{figure}

Most of the plants have these six parts that allow them to grow, reproduce and spread but shapes and functions can vary from one type of plant to another.

\section{Plant needs}

Depending on the type of plant and the environment in which the plant is growing, its needs can vary. However, some factors seems common to most of the plants and are described here.

\subsection{Nutriments}

The plant gets its nutriments mostly from the soil and the air. There are seventeen most important nutriments that contribute to the plant's growth \cite{plant-nutrition}:

\begin{multicols}{3}
    \begin{itemize}
        \item \Gls{n}
        
        \item \Gls{p}
        
        \item \Gls{k}
        
        \item \Gls{ca}
        
        \item \Gls{s}
        
        \item \Gls{mg}
        
        \item \Gls{c}
        
        \item \Gls{o}
        
        \item \Gls{h}
        
        \item \Gls{fe}
        
        \item \Gls{b}
        
        \item \Gls{cl}
        
        \item \Gls{mn}
        
        \item \Gls{zn}
        
        \item \Gls{cu}
        
        \item \Gls{mo}
        
        \item \Gls{ni}
        
        \item \Gls{co2}
    \end{itemize}
\end{multicols}

Nutriments are like vitamins to humans: abundance of nutriments can cause problems to the plant's growth and an abundance of one nutriment can cause deficiency of another. The right amount of nutriments can vary and are considered to be one of the most important aspects to the plant's health and yield.

\subsection{Water}

Water enables the seed to germinate and allows the dissolved sugar and the nutriments to navigate inside the plant. The roots absorb the water from the soil. However, there can be three main situations that can cause problems \cite{how-does-water-affect-plant-growth}\cite{water-important-plants}:

\begin{itemize}
    \item If there is too much water, the roots can rot and cannot get enough oxygen.
    
    \item If there is not enough water, the plant cannot absorb the nutriments from the soil.
    
    \item The soil cannot retain water and it goes away too quickly for the plant to get what it needs.
\end{itemize}

The right balance of water is the key to allow the plant to get its nutriments without being drowned.

\subsection{Soil}
A healthy soil should give every nutriment the plant needs naturally. \bigskip

A soil too compacted or too wet can prevent the roots to grow correctly and get its nutriments \cite{improving-compacted-soil}. A way to loose compacted soil is by adding organic materials such as worms and compost that allow oxygen and water to circulate. \bigskip

During the last decades, intensive farming has compacted the soils and compensated the lack of nutriments by using fertilizers and pesticides to overcome the decreasing health of the soils, the main sources of all the necessary nutriments presented above (see Figure~\ref{fig:heavy-machinery-in-field}).

\begin{figure}[!htbp]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{introduction/richard-bell-760823-unsplash}
        \caption*{\small\textit{Photo by Richard Bell} \cite{richard-bell}}
    \end{subfigure}
    \caption{Heavy machinery can harm the soil as it tends to compact it.}
    \label{fig:heavy-machinery-in-field}
\end{figure}

\subsection{Light}

Plants get energy from light through a process called photosynthesis. Photosynthesis is a process that use \gls{co2}, water and light to transform them into oxygen and glucose \cite{photosynthesis-for-kids}. \bigskip

The amount of light, its power and the light spectrum can affect the plant's growth \cite{how-light-affects-the-growth-of-a-plant}:

\begin{itemize}
    \item The amount of light (implied duration), how long a plant should have some sun (see Figure~\ref{fig:different-light-environments}).
    
    \item The power of the light source. A light that is too powerful can burn or over stress the plant \cite{cannabis-light-burn}.
    
    \item Some plants can grow differently based on the specific light spectrum it gets during the growth \cite{color-spectrum-marijuana-grow-light}.
\end{itemize}

\begin{figure}[!htbp]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{introduction/zetong-li-1375895-unsplash}
        \caption*{\small\textit{Photo by Zetong Li} \cite{zetong-li}}
        \caption*{A non-controlled light environment.}
    \end{subfigure}
    ~ 
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{introduction/two-hydroponic-cannabis-plants}
        \caption*{\small\textit{Photo by Lonehexagon} \cite{lonehexagon}}
        \caption*{A controlled light environment.}
    \end{subfigure}
    \caption{Different light environments.}
    \label{fig:different-light-environments}
\end{figure}

\subsection{Air and wind}

Air contains \gls{co2} and \gls{h} that are used by the plant for the photosynthesis process. \gls{co2} is a bit controversial as experiments have shown it can increase yields and quality but other factors should be considered first before seeing improvements \cite{does-rising-co2-benefit-plants}\cite{co2-for-growing-marijuana-plants}. \bigskip

At night, the plant produces \gls{o}. As a result of this, the plant must always get fresh air to be able to get some \gls{co2} and \gls{h} at day and evacuate the \gls{o} at night. This is the role of the wind. \bigskip

A wind too strong can obviously cause damages but it helps to pollinate the flowers, contributes to evaporation and change the air of the plant.

\subsection{Temperature}

Having extreme temperatures can lead to some weakness as the plant does not have the sufficient energy to survive that environment \cite{temperature-extremes-effect-on-planth-groth-and-development}. \bigskip

If the temperature is too high, the plant might get dry, even with sufficient water, as the plant tries to regulate its temperature and take energy to do so. On the other side, if the temperature is too low, the plant might go in a ``stand-by'' mode to protect itself from freezing and slow its growth. \bigskip

There seems that the difference between day and night times have a great influences on the plant's growth \cite{how-air-temperature-affects-plants}: the bigger the difference, the better the plant grows as it helps it to have a clear cycle between day and night, where the plant transports sugars to the warmer parts of the plants --- the fruits --- which need energy to grow.

\subsection{Humidity}

A plant transpires through its leaves. Humidity can help to regulate the transpiration but too much humidity can lead to fungus, parasites and the inability to evacuate the transpiration correctly. \bigskip

On the other side, a low humidity can lead to dryness and make the plant transpire more, using some of its energy trying to regulate.

\subsection{Space}

Plants need space to be able to grow. If the space is insufficient, the roots cannot grow properly and cannot get the nutriments correctly. Confined plants tend to stay small and produce small yields as they cannot develop correctly.

\subsection{Time}

Most plants need time from the state of seed to the state of plant. Many plants can live many seasons but plants used in agriculture are seasonal and often die at the end of the season.

\section{Weather and environment}

The weather and the environment in which the plants operate should provide the plants for all their needs. For hundred years, the weather has been essentially the same and the plants have grown under similar conditions year after year. \bigskip

However, for the last two centuries, industrialization has drastically changed the weather conditions. It is now hard to predict how the weather will be from one year to another and crops tend to suffer from the extremes temperatures and inappropriate environment.

\section{Seasons}

Seasons vary around the world (see Figure~\ref{fig:different-seasons-around-the-world}): in temperate and subpolar regions, there are four seasons: spring, summer, autumn and winter while in most tropical countries there are two seasons: wet and dry. \bigskip

\begin{figure}[!htbp]
    \centering
    \begin{subfigure}[b]{0.55\textwidth}
        \includegraphics[width=\textwidth]{introduction/seasons-map-data}
        \caption*{\small\textit{Schema by Julie Ramsden at ABC} \cite{julie-ramsden}}
    \end{subfigure}
    \caption{Different seasons around the world.}
    \label{fig:different-seasons-around-the-world}
\end{figure}

Seasons depend on the earth's position, the localization and regional climate and influence how the farmers organize themselves to produce food and which kind of food it is possible to grow.

\section{Crop varieties}

Over the years, crops have been genetically selected for their properties: crop yields, nutriments they contain, resistance to diseases, how many needs are required to grow, etc. \bigskip

Crop varieties are sometimes open source \cite{open-source-seed-initiative}\cite{opensourceseeds} (see Figure~\ref{fig:seeds-available-for-everyone}) but most of them are patented or even genetically created (\gls{gmo}) for their unique properties as the variety can make a major difference.

\begin{figure}[!htbp]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{introduction/open-source-initiative-seeds}
        \caption*{\small\textit{Photo by Open Source Seed Initiative} \cite{open-source-seed-initiative}}
    \end{subfigure}
    \caption{Some initiatives make their seeds available for everyone to use and improve.}
    \label{fig:seeds-available-for-everyone}
\end{figure}

\section{Crop rotation}

Crop rotation is a process to grow different crops on a given area to avoid soil impoverishment. This seasonal process gets better yields but also improve the soil structure by returning to the soil some nutriments that were taken from a given type of plant the season before with another type of plant \cite{crop-rotation}.

\section{Pesticides, fertilizers and other human interventions}

As mentioned at the beginning of this document, humans have shaped its environment to fulfilled its own needs \cite{anthropocene}. For decades, humans have acquired skills to produce food without any limitations. However, today, resources become more and more limited and agriculture is seeing a decrease in product quality \cite{a-decline-in-the-nutritional-value-of-crops}. \bigskip

Intensive farming uses a large amount of pesticides, fertilizers and machines to overcome the decreasing yields to produce our food as the soils have less and less nutriments and plants are more subject to pests. This causes environmental issues such as biodiversity collapses, soils degradation \cite{soil-erosion-and-degradation}, water pollution, etc. \bigskip

\section{Biodiversity}

Apart from the plant perspective, biodiversity surrounding the plant's environment can have an influence on the plant's growth. \bigskip

Parasites, rodents as well as pests among others can cause damage to plantations. However, insects such as bees, wasps and butterflies are essential to fertilize the flowers. \bigskip

Plants grouping or plant companion is the idea to associate plants that naturally help each other to grow \cite{permaculture1}\cite{permaculture-companion-planting}\cite{top-ten-companion-plants} and create an ecosystem which is resistant without human intervention. For example, good plants association such as tomatoes and basil can help each other by keeping humidity in the soil (tomatoes), protect from pests (pests do not like the smell of the basil plant), fight fungus (as the humidity level is well-balanced), etc. \bigskip

\begin{figure}[!htbp]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{introduction/fava-hillside}
        \caption*{\small\textit{Photo by Zoey Kroll} \cite{zoey-kroll}}
    \end{subfigure}
    \caption{Here the soil has been up-cycled from the urban waste stream.}
    \label{fig:soil-up-cycled-from-the-urban-waste-stream}
\end{figure}

Such as in Figure~\ref{fig:soil-up-cycled-from-the-urban-waste-stream}, there are many ways to create ecosystems that are resilient and produce great yields by creating a vast biodiversity nearly without human intervention with permaculture for example.

\section{A plant's life: the whole process}

Generally speaking, the process of a plant from the seed to the fruits is the following:

\begin{enumerate}
    \item The seeds are sowed in the fields during the spring, when the weather become warmer and warmer with a lot of rain.
    
    \item Water make the seeds sprout and roots develop in the soil.
    
    \item The first leaves appear.
    
    \item The plants grow having more and more leaves and developing flowers.
    
    \item Flowers bloom and insects as well as the wind fertilize the flowers.
    
    \item The fertilized flowers transform into fruits at the beginning of the summer.
    
    \item The weather becomes more and more hot with less raining periods. Fruits develop and become bigger.
    
    \item At the end of summer, the fruits are ripe and are ready for harvesting.
    
    \item The farmer harvests the fruits and the plant's life is considered done.
\end{enumerate}

Most plants do not follow this exact pattern, and their starting and ending can be shifted and plants like trees or onions repeat this kind of process every year if left untouched. \bigskip

Spring-summer-autumn is the most common pattern but some plants grow during the winter and a well organized farmer can grow food all year, sometimes with the help of greenhouses. \bigskip

\section{Conclusions}

Plants might seems like simple organisms. Although, as seen in the previous sections, many factors can influence their health and how they are able to grow. \bigskip

Regarding the information collected, some key elements seems most important to the life-cycle of a plant (in no particular order):

\begin{multicols}{2}
    \begin{itemize}
        \item Nutriments coupled with the soil
        
        \item Water
        
        \item Light
        
        \item Temperature

        \item Humidity

        \item[]
    \end{itemize}
\end{multicols}

Other key elements are important as well (mostly the biodiversity element) but most people take care --- in a very simplistic approach --- of their plants only by watching the amount of water, daylight, temperature and humidity needed. \bigskip

The next step is to find the right data needed for this work:

\begin{itemize}
    \item Data about weather condition (combining water, light, temperature and more).
    
    \item Data about soils.
    
    \item Data about plant's yields.
\end{itemize}
