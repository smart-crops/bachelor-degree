Preprocessing is an important part of machine learning. Not only the data should be constructed to fully represents the problem to solve but preprocessing the data can help the algorithms to create an accurate model more quickly. \bigskip

These steps can be done many times depending on the problem to be solved and most of them can be automated when the datasets are constructed.

\section{Preprocessing pipeline}

The usual pipeline for preprocessing the data is the following one:

\begin{multicols}{2}
    \begin{enumerate}
        \item Data formatting and cleaning.
        
        \item Datasets construction.
        
        \item Datasets under and over sampling.
    
        \item Defining training and test datasets.
        
        \item Data scaling.

        \item[]
    \end{enumerate}
\end{multicols}

\subsection{Data formatting and cleaning}

This step consists in removing unnecessary data or in formating them so the data is consistent in the entire dataset.

\subsection{Datasets construction}

The problem needing to be solved determines how data should be structured. \bigskip

Merging and performing calculations on the data are part of the dataset construction to completely model the problem.

\subsection{Datasets under and over sampling}

The number of elements contained in datasets can be sometimes too high or not enough for the needed experiment. Under and over sampling are techniques to reduce or create new data based on the ones already available.

\subsection{Defining training and test datasets}

When datasets are ready, a part of the data will be used to train the model and the other will be used to test the created model to validate the performance of it \cite{training-validation-and-test-sets}. \bigskip

A common way to train and test the model is cross-validation \cite{cross-validation}. \bigskip

The concept is to take the entire dataset and split it in the usual training/testing datasets but doing it iteratively (see Figure~\ref{fig:k-folds-cross-validation}): at the end, the model will be trained and tested using the entire dataset and that should be better than only using a one-time training dataset and a one-time testing dataset.

\begin{figure}[!htbp]
    \centering
    \begin{subfigure}[b]{0.75\textwidth}
        \includegraphics[width=\textwidth]{data-preprocessing-and-exploration/k-folds-cross-validation}
        \caption*{\small\textit{Schema by Joseph Nelson} \cite{joseph-nelson}}
    \end{subfigure}
    \caption{K-Folds cross validation process.}
    \label{fig:k-folds-cross-validation}
\end{figure}

\subsection{Data scaling}

When comparing two independent variables or features, the scale can be difficult to interpret as the range or the units are too different to easily understand their correlation. For example, comparing ages with month's salaries can be difficult as the age's range is often way smaller than the salaries \cite{normalize-standardize-time-series-data-python}. \bigskip

Data scaling must be applied to the training and test datasets independently to avoid biasing the model during the learning process \cite{lstm-autoencoder-for-extreme-rare-event-classification-in-keras}.

\subsubsection{Data normalization}

Data normalization \cite{what-is-normalization-for} - also called feature scaling \cite{feature-scaling} - is a method to scale the variables between a min and a max for all scales to compare them and helps the algorithm to learn faster as all scales are normalized \cite{why-data-normalization-is-necessary-for-machine-learning-models}: \bigskip

\begin{center}
    $x_{normalized} = \frac{x - x_{min}}{x_{max} - x_{min}}$
\end{center}

\subsubsection{Data standardization}

Data standardization is another way to compare independent variables or features based on the \gls{mean} and the \gls{standard-deviation}: \bigskip

\begin{center}
    $x_{standardized} = \frac{x - \mu}{\sigma}$
\end{center}

Standardization considers outliers better than normalization but both methods help the algorithm but one can give better results than the other depending on how algorithms can take benefits from the two kind of values \cite{what-s-the-difference-between-normalization-and-standardization} \cite{standardization-vs-normalization}. \bigskip

Some tests should be done to determine the best scaling method to use for the given problem \cite{normalization-vs-standardization-quantitative-analysis}.

\section{Preprocessing pipeline applied to the data}

Some \gls{python} scripts have been written to simplify the preprocessing and exploration of the data. This allows to quickly make new experiments and directly save the results.

\subsection{Import and prepare crop dataset}

As only a single crop is experimented at a time, the crop dataset is imported and is filtered to keep only the needed crop and the available years for the given crop (Listings~\ref{lst:import-crop-dataset} and \ref{lst:clean-crop-dataset}).

\lstinputlisting[
    language  = {Python},
    style     = {numbers},
    caption   = {\texttt{\lstname}: Import the crop dataset.},
    label     = {lst:import-crop-dataset},
    linerange = {import_crop_dataset_begin-import_crop_dataset_end},
]{experience.py}

\lstinputlisting[
    language  = {Python},
    style     = {numbers},
    caption   = {\texttt{\lstname}: Clean the crop dataset.},
    label     = {lst:clean-crop-dataset},
    linerange = {clean_crop_dataset_begin-clean_crop_dataset_end},
]{experience.py}

\subsection{Create crop seasons dataset} \label{section:create-crop-seasons-dataset}

Both crops are sowed between September and November and are harvested between summer and early autumn the next year. As such, weather data must be taken into account with the sowing/harvesting dates for each crop. \bigskip

In the Listing~\ref{lst:create-crop-seasons-dataset}, a last column is added to know how many data is available for the given period depending on the resampling of the weather data.

\lstinputlisting[
    language  = {Python},
    style     = {numbers},
    caption   = {\texttt{\lstname}: Create crop seasons dataset.},
    label     = {lst:create-crop-seasons-dataset},
    linerange = {create_crop_seasons_dataset_begin-create_crop_seasons_dataset_end},
]{experience.py}

\subsection{Import weather dataset} \label{section:import-weather-dataset}

As weather data has already been cleaned and prepared in Section~\ref{section:weather-data}, the data is only imported as it in Listing~\ref{lst:import-weather-dataset}. Only wanted weather variables are imported for further customization.

\lstinputlisting[
    language  = {Python},
    style     = {numbers},
    caption   = {\texttt{\lstname}: Import and prepare weather dataset.},
    label     = {lst:import-weather-dataset},
    linerange = {import_weather_dataset_begin-import_weather_dataset_end},
]{experience.py}

\subsection{Split weather dataset by crop season}

Using the datasets prepared in Sections~\ref{section:create-crop-seasons-dataset} and \ref{section:import-weather-dataset}, only the required weather data is taken for each crop season (Listing~\ref{lst:split-by-crop-season}).

\lstinputlisting[
    language  = {Python},
    style     = {numbers},
    caption   = {\texttt{\lstname}: Split weather dataset by crop season.},
    label     = {lst:split-by-crop-season},
    linerange = {split_by_crop_season_begin-split_by_crop_season_end},
]{experience.py}

\subsection{Resample weather data by crop season} \label{section:resample-weather-data-by-crop-season}

As weather data is given hourly from IDAWEB, this can be too much for some first experiments. Resampling the weather data to have fewer data for quick testing is a better solution, as presented in Listing~\ref{lst:resample}. \bigskip

The data is resampled by mean and some available resamplings are by hours, days, weeks, months or years. \bigskip

Depending on the period of the years needed to be resampled (leap years, ripeness time over many years, etc.) the amount of measurements per crop season is not the same for each crop season. This is why only common dates over the crop seasons are kept to have the right amount of data and compare the crop seasons with all the same measurements at the same time of the season.

\lstinputlisting[
    language  = {Python},
    style     = {numbers},
    caption   = {\texttt{\lstname}: Resample weather data by crop season.},
    label     = {lst:resample},
    linerange = {resample_begin-resample_end},
]{experience.py}

\subsection{Transform weather data to stationary data}

Time series can cause problems for machine learning if they have trends that alter the learning process in an unwanted way. Transforming time series into stationary data suppresses the trends that can be confusing for machine learning by keeping the differences between $t-1$ and $t$ for all the dataset. \bigskip

Listing~\ref{lst:transform-to-stationary-data} shows how regular time series are transformed.

\lstinputlisting[
    language  = {Python},
    style     = {numbers},
    caption   = {\texttt{\lstname}: Transform weather data to stationary data.},
    label     = {lst:transform-to-stationary-data},
    linerange = {transform_to_stationary_data_begin-transform_to_stationary_data_end},
]{experience.py}

\subsection{Split datasets into training and test datasets and scaling}

Listing~\ref{lst:scale-weather-data} uses a scaler that can either normalize or standardize the weather data using all the available stationary data on each crop season. \bigskip

Indexes are lost during this operation, so they are put back to still be able to make some filtering based on dates. \bigskip

\lstinputlisting[
    language  = {Python},
    style     = {numbers},
    caption   = {\texttt{\lstname}: Scale weather data.},
    label     = {lst:scale-weather-data},
    linerange = {scale_begin-scale_end},
]{experience.py}

Listing~\ref{lst:split-by-folds} shows how the datasets are split using K-Fold cross-validation. The training and test datasets are scaling independently.

\lstinputlisting[
    language  = {Python},
    style     = {numbers},
    caption   = {\texttt{\lstname}: Split datasets using K-Fold cross-validation.},
    label     = {lst:split-by-folds},
    linerange = {split_by_folds_begin-split_by_folds_end},
]{experience.py}

\section{Data exploration}

\subsection{Yields}

Figure~\ref{fig:yield-average-per-crop} shows the yield average for all crops and the following points can be noticed from the figure:

\begin{itemize}
    \item Regarding \gls{winter-wheat}, the three best yield years are 2002 (\SI{49.20}{\deca\tonne}), 2004 (\SI{41.91}{\deca\tonne}) and 2011 (\SI{40.48}{\deca\tonne}). The three worst yield years are 2013 (\SI{21}{\deca\tonne}), 2016 (\SI{25.35}{\deca\tonne}) and 2001 (\SI{27.43}{\deca\tonne}).

    \item Regarding \gls{winter-field-beans}, the three best yield years are 2008 (\SI{37.15}{\deca\tonne}), 2005 (\SI{35.66}{\deca\tonne}) and 2007 (\SI{33.55}{\deca\tonne}). The three worst yield years are 2014 (\SI{19.30}{\deca\tonne}), 2003 (\SI{20.81}{\deca\tonne}) and 2017 (\SI{22.85}{\deca\tonne}).

    \item In 2003 and 2015, \gls{winter-wheat} has produced the same amount but \gls{winter-field-beans} has a ratio difference of $\approx 1.4$ yield difference between the two crops.

    \item In 2004 and 2013, \gls{winter-field-beans} has produced the same amount but \gls{winter-wheat} has a ratio difference of $\approx 2$ yield difference between the two crops.

    \item In 2004 and 2016, both crop has produced the same amount with a ratio difference of $\approx 1.05$ yield difference between the two crops.

    \item 2013 is the only year where \gls{winter-field-beans} has a higher yield than \gls{winter-wheat}.
\end{itemize}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=\textwidth, height=6.5cm,]{plots/yield-average-per-crop.tikz}
    \caption{Yield average per crop in [\SI{}{\deca\tonne}] (2000-2017).}
    \label{fig:yield-average-per-crop}
\end{figure}

Tables~\ref{table:winter-wheat-beans-crop-seasons} and \ref{table:winter-field-beans-crop-seasons} 
recapitulate the sowing and harvesting dates as well as the yield related to this period for each crop.

\begin{table}[!htbp]
    \centering
    \csvreader[
        tabular={
            l
            l
            S[table-format=2.2]
        },
        table head=\toprule {Sowing date} & {Harvesting date} & {Yield [\SI{}{\deca\tonne}]} \\\midrule,
        table foot=\bottomrule,
        separator=semicolon
    ]{data/plots/winter-wheat/crop-seasons.csv}{
        2=\SowingDate,
        3=\HarvestingDate,
        4=\Yield
    }{\SowingDate & \HarvestingDate & \Yield}
    \caption{\acs{winter-wheat} crop seasons (2000-2017).}
    \label{table:winter-wheat-beans-crop-seasons}
\end{table}
\begin{table}[!htbp]
    \centering
    \csvreader[
        tabular={
            l
            l
            S[table-format=2.2]
        },
        table head=\toprule {Sowing date} & {Harvesting date} & {Yield [\SI{}{\deca\tonne}]} \\\midrule,
        table foot=\bottomrule,
        separator=semicolon
    ]{data/plots/winter-field-beans/crop-seasons.csv}{
        2=\SowingDate,
        3=\HarvestingDate,
        4=\Yield
    }{\SowingDate & \HarvestingDate & \Yield}
    \caption{\acs{winter-field-beans} crop seasons (2000-2017).}
    \label{table:winter-field-beans-crop-seasons}
\end{table}

\subsection{Weather variables}

Figures~\ref{fig:winter-wheat-good-and-bad-yield-temp-radi-time-series} and \ref{fig:winter-wheat-good-and-bad-yield-prec-humi-time-series} show the time series for the weather variables for good and bad yield years. \bigskip

Regarding the good yield years, the following elements can be noticed:

\begin{itemize}
    \item \textbf{Air temperature}: From week 28 to week 30, it seems the temperature is dropping a bit for the three best years compared to the three worst years.

    \item \textbf{Global radiation}: From week 8 to week 14, it seems the radiation should increase progressively and between week 13 and week 16, the radiation is more elevated than for the worst years. From week 21 to week 23, all the three best years see a drop in terms of radiation and follow the exact same pattern.

    \item \textbf{Precipitation}: From week 3 to week 6, the best three years show less precipitation than bad years and the same pattern can be seen between week 13 and 16. One thing to notice is the fact that between week 25 and 29, both good and bad years follow quite the same pattern. It could mean it does not matter at all whether it's a good or a bad year.

    \item \textbf{Relative humidity}: From week 48 to week 1, the relative humidity drops for the three best years. From week 8 to week 13, the relative humidity decreases progressively and is lower than for bad years. Between weeks 21 and 26, the three best years show a increase in the relative humidity directly followed by a decrease that is lower than the worst years.
\end{itemize} 

Regarding the bad yield years, the new following elements can be noticed:

\begin{itemize}
    \item \textbf{Air temperature}: From week 3 to week 4, the temperature rises a little compared to the good years.

    \item \textbf{Global radiation}: From week 9 to week 13, the global radiation is lower than for the good years.

    \item \textbf{Precipitation}: From week 51 to week 2, the precipitation shows a great increase for two of the worst years. It is interesting to notice that one of the three best years shows the same pattern but with a week offset.
\end{itemize}

Figure~\ref{fig:winter-wheat-good-and-bad-yield-weather-variables-comparison} compares the different weather variables between good and bad yield years for \gls{winter-wheat} considering the data during the entire year. \bigskip

The following points can be noticed from the plots:

\begin{itemize}
    \item On average, years with lower air temperature tend to give better yield.

    \item On average, years with more global radiation tend to give better yield.

    \item On average, years with lower precipitation tend to give better yield.

    \item On average, years with lower relative humidity tend to give better yield.
\end{itemize}

\begin{sidewaysfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textheight, height=0.25\textwidth]{plots/winter-wheat-air-temperature-at-2m-above-ground-time-series.tikz}
        \caption*{Air temperature at \SI{2}{\meter} above ground time series.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textheight, height=0.25\textwidth]{plots/winter-wheat-global-radiation-time-series.tikz}
        \caption*{Global radiation time series.}
    \end{subfigure}

    \caption{Good and bad yield years air temperature and global radiation time series resampled by weeks for \acs{winter-wheat}.}
    \label{fig:winter-wheat-good-and-bad-yield-temp-radi-time-series}
\end{sidewaysfigure}

\begin{sidewaysfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textheight, height=0.25\textwidth]{plots/winter-wheat-precipitation-time-series.tikz}
        \caption*{Precipitation time series.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textheight, height=0.25\textwidth]{plots/winter-wheat-relative-humidity-time-series.tikz}
        \caption*{Relative humidity time series.}
    \end{subfigure}

    \caption{Good and bad yield years precipitation and relative humidity time series resampled by weeks for \acs{winter-wheat}.}
    \label{fig:winter-wheat-good-and-bad-yield-prec-humi-time-series}
\end{sidewaysfigure}

\begin{figure}[!htbp]
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=0.2\textheight]{plots/winter-wheat-air-temperature-at-2m-above-ground-comparison.tikz}
        \caption*{Air temperature at \SI{2}{\meter} above ground comparison.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=0.2\textheight]{plots/winter-wheat-global-radiation-comparison.tikz}
        \caption*{Global radiation comparison.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=0.2\textheight]{plots/winter-wheat-precipitation-comparison.tikz}
        \caption*{Precipitation comparison.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=0.2\textheight]{plots/winter-wheat-relative-humidity-comparison.tikz}
        \caption*{Relative humidity comparison.}
    \end{subfigure}

    \caption{Good and bad yield years weather variables comparison resampled by weeks for \acs{winter-wheat}.}
    \label{fig:winter-wheat-good-and-bad-yield-weather-variables-comparison}
\end{figure}

Figures~\ref{fig:winter-field-beans-good-and-bad-yield-temp-radi-time-series} and \ref{fig:winter-field-beans-good-and-bad-yield-prec-humi-time-series} show the time series for the weather variables for good and bad yield years. \bigskip

Regarding the good yield years, the following elements can be noticed:

\begin{itemize}
    \item \textbf{Air temperature}: From week 45 to week 50, the temperature is more elevated than for the bad years. In contrast, the three following weeks show an opposite trend where the temperature should be lower than the worst years. From week 21 to week 25, the temperature is lower than for the worst years.

    \item \textbf{Global radiation}: From week 11 to week 17, it seems the global radiation should stay lower than for bad years and increase progressively. Between week 20 and 23, the global radiation is lower than the bad years and should remain until week 25.

    \item \textbf{Precipitation}: Regarding the precipitation, not much can be determined for \gls{winter-field-beans}. The only noticeable element is between weeks 46 and 50 where the precipitation is greater than for the bad years. The rest of the data is pretty chaotic and no other visual patterns can be found.

    \item \textbf{Relative humidity}: Between weeks 48 and 1, the relative humidity is lower for the good years. The good years follow mostly the same pattern for the rest of the crop season.
\end{itemize} 

Regarding the bad yield years, the new following elements can be noticed:

\begin{itemize}
    \item \textbf{Global radiation}: Between weeks 11 and 15, the global radiation is higher than for the good years.
\end{itemize}

Figure~\ref{fig:winter-field-beans-good-and-bad-yield-weather-variables-comparison} compares the different weather variables between good and bad yield years for \gls{winter-field-beans} considering the data during the entire year. The following points can be interpreted from the plots:

\begin{itemize}
    \item On average, years with lower air temperature tend to give better yield.

    \item On average, years with less global radiation tend to give better yield.

    \item No specific pattern can be deduced for precipitation so this weather variable could be tossed if needed.

    \item Years with humidity $\approx \SI{70}{\percent}$ tend to give better yield. Years with higher or lower than \SI{70}{\percent} tend to give worse yield.
\end{itemize}

\begin{sidewaysfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textheight, height=0.25\textwidth]{plots/winter-field-beans-air-temperature-at-2m-above-ground-time-series.tikz}
        \caption*{Air temperature at \SI{2}{\meter} above ground time series.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textheight, height=0.25\textwidth]{plots/winter-field-beans-global-radiation-time-series.tikz}
        \caption*{Global radiation time series.}
    \end{subfigure}

    \caption{Good and bad yield years air temperature and global radiation time series resampled by weeks for \acs{winter-field-beans}.}
    \label{fig:winter-field-beans-good-and-bad-yield-temp-radi-time-series}
\end{sidewaysfigure}

\begin{sidewaysfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textheight, height=0.25\textwidth]{plots/winter-field-beans-precipitation-time-series.tikz}
        \caption*{Precipitation time series.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textheight, height=0.25\textwidth]{plots/winter-field-beans-relative-humidity-time-series.tikz}
        \caption*{Relative humidity time series.}
    \end{subfigure}

    \caption{Good and bad yield years precipitation and relative humidity time series resampled by weeks for \acs{winter-field-beans}.}
    \label{fig:winter-field-beans-good-and-bad-yield-prec-humi-time-series}
\end{sidewaysfigure}

\begin{figure}[!htbp]
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=0.2\textheight]{plots/winter-field-beans-air-temperature-comparison.tikz}
        \caption*{Air temperature at \SI{2}{\meter} above ground comparison.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=0.2\textheight]{plots/winter-field-beans-global-radiation-comparison.tikz}
        \caption*{Global radiation comparison.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=0.2\textheight]{plots/winter-field-beans-precipitation-comparison.tikz}
        \caption*{Precipitation comparison.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=0.2\textheight]{plots/winter-field-beans-relative-humidity-comparison.tikz}
        \caption*{Relative humidity comparison.}
    \end{subfigure}

    \caption{Good and bad yield years weather variables comparison resampled by weeks for \acs{winter-field-beans}.}
    \label{fig:winter-field-beans-good-and-bad-yield-weather-variables-comparison}
\end{figure}

\subsection{Correlations between weather variables}

Figure~\ref{fig:correlation-matrix-between-weather-variables-resampled-by-weeks-for-winter-wheat} shows how weather variables are correlated between them. The differences between the two matrices are the precipitation-air temperature and the relative humidity-precipitation correlations. \bigskip

\begin{figure}[!htbp]
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics{plots/winter-wheat-correlation-matrix-for-the-best-year.tikz}
        \caption*{Correlation matrix for the best yield (year 2002).}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics{plots/winter-wheat-correlation-matrix-for-the-worst-year.tikz}
        \caption*{Correlation matrix for the worst yield (year 2013).}
    \end{subfigure}

    \caption{Correlation matrix between weather variables resampled by weeks for \acs{winter-wheat}.}
    \label{fig:correlation-matrix-between-weather-variables-resampled-by-weeks-for-winter-wheat}
\end{figure}

The differences between the two matrices for Figure~\ref{fig:correlation-matrix-between-weather-variables-resampled-by-weeks-for-winter-field-beans} are the precipitation-global radiation, precipitation-air temperature and relative humidity-precipitation correlations. \bigskip

\begin{figure}[!htbp]
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics{plots/winter-field-beans-correlation-matrix-for-the-best-year.tikz}
        \caption*{Correlation matrix for the best yield (year 2008).}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics{plots/winter-field-beans-correlation-matrix-for-the-worst-year.tikz}
        \caption*{Correlation matrix for the worst yield (year 2017).}
    \end{subfigure}

    \caption{Correlation matrix between weather variables resampled by weeks for \acs{winter-field-beans}.}
    \label{fig:correlation-matrix-between-weather-variables-resampled-by-weeks-for-winter-field-beans}
\end{figure}

\subsection{Differences between the two crops environments}

Figure~\ref{fig:differences-between-winter-wheat-and-winter-field-beans-environments} compares the best, the same and the worst yield years between \gls{winter-wheat} and \gls{winter-field-beans} to spot environmental differences.

\begin{itemize}
    \item \gls{winter-wheat} prefers hotter environments than \gls{winter-field-beans}.

    \item \gls{winter-wheat} prefers more global radiation than \gls{winter-field-beans}.

    \item \gls{winter-wheat} prefers to have less precipitation than \gls{winter-field-beans}.

    \item \gls{winter-wheat} prefers to have more humidity than \gls{winter-field-beans}.
\end{itemize}

For most of the points mentioned above, the plots show that the opposite is also true for poor harvest years. \bigskip

\begin{figure}[!htbp]
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=0.2\textheight]{plots/both-air-temperature-at-2m-above-ground-comparison.tikz}
        \caption*{Air temperature at \SI{2}{\meter} above ground comparison.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=0.2\textheight]{plots/both-radiation-comparison.tikz}
        \caption*{Global radiation comparison.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=0.2\textheight]{plots/both-precipitation-comparison.tikz}
        \caption*{Precipitation comparison.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textwidth, height=0.2\textheight]{plots/both-humidity-comparison.tikz}
        \caption*{Relative humidity comparison.}
    \end{subfigure}

    \caption{Differences between \acs{winter-wheat} and \acs{winter-field-beans} environments with weather variables resampled by weeks.}
    \label{fig:differences-between-winter-wheat-and-winter-field-beans-environments}
\end{figure}

\subsection{Regular, stationary and normalized time series}

Figure~\ref{fig:differences-between-regular-stationary-and-scaled-time-series} shows the differences between regular time series, stationary time series and scaled time series for air temperature at \SI{5}{\centi\meter} above ground.

\begin{sidewaysfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textheight, height=0.17\textwidth]{plots/air-temperature-regular-time-series.tikz}
        \caption*{Regular time series.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textheight, height=0.17\textwidth]{plots/air-temperature-stationary-time-series.tikz}
        \caption*{Stationary time series.}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\textheight, height=0.17\textwidth]{plots/air-temperature-scaled-time-series.tikz}
        \caption*{Scaled time series between $-1$ and $1$.}
    \end{subfigure}

    \caption{Differences between regular, stationary and scaled time series for \acs{winter-wheat} crop season 2016-2017 resampled by weeks.}
    \label{fig:differences-between-regular-stationary-and-scaled-time-series}
\end{sidewaysfigure}

\section{Conclusions}

As seen in the previous sections, \gls{winter-wheat} and \gls{winter-field-beans} have their very own specific environments to grow. \bigskip

According to the data, it should be possible to train an \gls{ai} model to model these crops environments and to be able to predict the yield for a given weather crop data.
