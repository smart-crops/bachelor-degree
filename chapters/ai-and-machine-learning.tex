Humans, throughout their lives, learn from trials and errors: based on the output of a life experience, humans will learn to take decisions that allow them to evolve and make better ones the next time they will be confronted to the same situation. \bigskip

Experience acquired can help to take better decisions in the future and \gls{machine-learning} is significantly the same process, based on data.

\section{An introduction to \gls{machine-learning}}

\Gls{machine-learning} is a subset of artificial intelligence. It is a process that teaches computers how to make predictions or decisions based on data in the same way humans learn: from experience \cite{mathworks-machine-learning}\cite{wikipedia-machine-learning}. \bigskip

\Gls{machine-learning} needs data to gain experience. The ``\textit{data life experiences}'' is called ``\gls{training-data}''. \Gls{training-data} is used to create the \gls{model} (the ``\textit{acquired data experience}'') and can then be used to make predictions or decisions. \bigskip

As data is the very essence of the construction of the \gls{model}, it is very important that it is properly labeled and cleaned of unnecessary data, called \gls{noisy-data} \cite{noisy-data}. However, data must not be sterile of improper data as the \gls{model} needs to generalize and find similarities even with data that can vary a little. \bigskip

The \gls{model} is then built with \gls{training-data}, a subset of all the available data, where the algorithm will try to find recurring events, patterns, similarities and build experience out of it. \bigskip

The \gls{model} must be then tested with new data, called ``\gls{test-data}'', the rest of the available data minus the \gls{training-data}. Looking at the outputs of the \gls{model}, there are three possible cases:

\begin{itemize}
    \item \Gls{over-fitted}: the \gls{model} cannot find the correct output with new data because it is too specialized. It has learned the \gls{training-data} ``by heart'' and is not able to inference new data.
    
    \item \Gls{under-fitted}: the \gls{model} cannot find the correct output with new data because it lacks knowledge to be able to inference new data.
    
    \item A good fit: the \gls{model} has the right balance of specialization and generalization. It is specialized enough to find the right patterns in data while not being too specialized and discarding data that do not fit exactly.
\end{itemize}

Over-fitted and under-fitted data is, of course, not what makes a good \gls{model}. The target of modeling is to find a good fit and the process can many iterations before finding the good fit for a given task. \bigskip

Sometimes, a validation set can be used to minimize over-fitting to stop the training if model becomes too over-fitted \cite{what-s-is-the-difference-between-train-validation-and-test-set-in-neural-networks}.

To summarize the process, here are the steps used to build a \gls{model} that can make predictions or decisions (see Figure~\ref{fig:training-validation-and-testing-datasets-usages}):

\begin{enumerate}
    \item Data is carefully gathered, cleaned and normalized for the algorithm.
    
    \item The algorithm is fed with \gls{training-data} and \gls{validation-data} to build a \gls{model}.
    
    \item The \gls{model} is tested with the \gls{test-data} to see if the \gls{model} fits correctly new data.
    
    \item Based on the results of the \gls{test-data}, the \gls{model} can be reworked to find a good fit.
    
    \item When satisfied with the \gls{model}, it can be used in production. Depending on the used algorithm, the \gls{model} can improve itself overtime with new data without having to completely rebuild a \gls{model}.
\end{enumerate}

\begin{figure}[!htbp]
    \centering
    \begin{subfigure}[b]{0.65\textwidth}
        \includegraphics[width=\textwidth]{ai-and-machine-learning/training-testing-vadilation-sets}
        \caption*{\small\textit{Schema by \gls{nthu}} \cite{nthu}}
    \end{subfigure}
    \caption{Training, validation and test datasets usages.}
    \label{fig:training-validation-and-testing-datasets-usages}
\end{figure}

\section{Promises and limitations}

\Gls{machine-learning} is a trendy word at the moment and must be used with caution as many projects use it more for marketing purpose than anything else. The possibilities as well as the limitations should be discussed regarding that topic. \bigskip

The applications of \gls{machine-learning} are multiple and can already outperform humans in some specific domains \cite{11-times-ai-beat-humans-at-games-art-law-and-everything-in-between}\cite{ai-artificial-intelligence-machine-learning-computational-science}\cite{alphastar-mastering-real-time-strategy-game-starcraft-ii}:

\begin{multicols}{3}
    \begin{itemize}
        \item Image detection.
        
        \item Financial prediction.
        
        \item Gaming (see Figure~\ref{fig:deepmind-alphastar-mastering-starcraft-ii}).
        
        \item Computational biology.
        
        \item Precise agriculture.
        
        \item etc.
    \end{itemize}
\end{multicols}

\Gls{machine-learning} seems to resolve any kind of tasks with the right amount of data and time. Experiments have shown us that it moves faster than we thought and that we have not reached the limits yet. \bigskip

\begin{figure}[!htbp]
    \centering
    \begin{subfigure}[b]{\textwidth}
        \includegraphics[width=\textwidth]{ai-and-machine-learning/sc2-agent-visualisation}
        \caption*{\small\textit{Schema by DeepMind} \cite{alphastar-mastering-real-time-strategy-game-starcraft-ii}}
    \end{subfigure}
    \caption{DeepMind's AlphaStar mastering StarCraft II against the player MaNa.}
    \label{fig:deepmind-alphastar-mastering-starcraft-ii}
\end{figure}

However, \gls{machine-learning} - and generally speaking artificial intelligence - still have limitations that differentiate them from the way humans think \cite{top-5-limitations-of-machine-learning-in-an-enterprise-setting}\cite{limitations-of-artificial-intelligence-mobile-marketing}\cite{chinese-woman-offered-refund-after-facial-recognition-allows}:

\begin{itemize}
    \item \Gls{machine-learning} requires a massive amount of data to build complete models.
    
    \item Even with the right amount of data, models cannot be generalized and stay in a specific field: at the moment, an \gls{ai} is made for a specific task and cannot teach itself to learn something completely new without being formed before.
    
    \item Models are made to respond to a specific type of data. If the data is biased, the model will be as well.
    
    \item \gls{ai} do not share knowledge or collaborate on their own. 
\end{itemize}

Some limitations cited are also seen in humans, but the learning curve is not limited: they can still learn new things and improve on their own in a totally new field without anyone helping them if they want to.

\section{Work carried out in the fields of agriculture and plants}

Agriculture is a vast field where a tremendous amount of work is done regarding \gls{machine-learning} \cite{ai-machine-learning-artificial-intelligence-agriculture-farming}\cite{how-is-machine-learning-used-in-agriculture}\cite{machine-learning-in-agriculture}\cite{deep-learning-in-agriculture-a-survey}\cite{applying-machine-learning-to-agriculture-data}\cite{ai-agriculture-present-applications-impact}\cite{machine-learning-for-big-data-analytics-in-plants}\cite{machine-learning-in-agriculture-a-review}. \bigskip

Here are some summaries of projects and studies that have been made in this field.

\subsection{Work 1: Flavor cyber agriculture: Optimization of plant metabolites in an open-source control environment through surrogate modeling}

This work \cite{flavor-cyber-agriculture-optimization-of-plant-metabolites-in-an-open-source-control-environment-through-surrogate-modeling} was carried in 2019 by searchers at the Massachusetts Institute of Technology. \bigskip

The searchers were able to model the best light conditions for basil plants. They wanted to find the best ones to get the best taste of the basil plant and not the biggest yields. Other variables other than light were the same for all plants, and they have built an in-house farming environment for the experiments. \bigskip

They were not only able to model the ideal light conditions for the basil plants, but they were able to find that basil plants give the best tasting flavor when they are exposed to a 24-hour regime.

\subsection{Work 2: Decision Support System for Greenhouse Tomato Yield Prediction using Artificial Intelligence Techniques}

This work \cite{decision-support-system-for-greenhouse-tomato-yield-prediction-using-artificial-intelligence-techniques} was carried in 2010 by searchers at the University of Warwick, UK. \bigskip

Using data from 1999 to 2000, they were able to model and predict the yields of tomatoes from week to week from a monitored greenhouse with daily measurements of the temperature, the \gls{co2} concentration, the solar radiation and the \gls{vpd}. \bigskip

The system can predict the tomatoes yields with an accuracy between 66\% and 83\%.

\subsection{Work 3: Synthesis of a greenhouse climate controller using AI-based techniques}

This work \cite{synthesis-of-a-greenhouse-climate-controller-using-ai-based-techniques} was carried in 1996 by searchers at the University of Catania. \bigskip

The searchers were able to model how to manage the greenhouse controller based on the data collected from the greenhouse sensors. \bigskip

The paper does not give many details but it shows that in 1996 already, searchers were using machine learning to solve this kind of problems. \bigskip

\section{Machine learning in this bachelor thesis}

In this work, machine learning will be used to build a model of a plant's life-cycle in its current environment. \bigskip

The purpose is to model the weather environment that leads to a given yield and be able to predict how much yield a crop will produce in this given environment. With a well-trained \gls{model}, the most important key factors should be kept to determine which weather variables are the most important and when.
