import argparse
import os

import datetime as dt
import numpy as np
import pandas as pd

from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import GRU
from keras.models import Sequential
from sklearn.model_selection import KFold
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler

import utils

from const import CROPS_FILE
from const import CROPS_FILE_COLS_NAMES
from const import CROPS_FILE_COLS_TYPES
from const import WEATHER_FILE
from const import WEATHER_FILE_COLS_NAMES
from const import WEATHER_FILE_COLS_TYPES
from const import CSV_SEPARATOR
from const import FILES_TEMPLATE
from const import MODELS_TEMPLATE
from const import DISPLAY_TEMPLATE

# Functions
"""parse_args_begin"""
def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--crop-to-experiment',
        required=True,
        type=str,
        dest='crop_to_experiment',
        help='the crop to experiment.',
    )

    parser.add_argument(
        '--crop-sowing-month',
        required=True,
        type=int,
        dest='crop_sowing_month',
        help='the crop sowing month.',
    )

    parser.add_argument(
        '--crop-sowing-day',
        required=True,
        type=int,
        dest='crop_sowing_day',
        help='the crop sowing day.',
    )

    parser.add_argument(
        '--crop-harvesting-month',
        required=True,
        type=int,
        dest='crop_harvesting_month',
        help='the crop harvesting month.',
    )

    parser.add_argument(
        '--crop-harvesting-day',
        required=True,
        type=int,
        dest='crop_harvesting_day',
        help='the crop harvesting day.',
    )

    parser.add_argument(
        '--crop-ripeness-time',
        required=True,
        type=int,
        dest='crop_ripeness_time',
        help='the crop ripeness time.',
    )

    parser.add_argument(
        '--weather-variables',
        required=True,
        nargs='+',
        dest='weather_variables',
        help='the weather variables to use with the crop.',
    )

    parser.add_argument(
        '--weather-resample-by',
        required=True,
        type=str,
        dest='weather_resample_by',
        help='how to resample the weather data (H, D, W, M or Y).',
    )

    parser.add_argument(
        '--weather-resample-fn-name',
        required=True,
        type=str,
        dest='weather_resample_fn_name',
        help='the resampling function to use (min, std, mean or max).',
    )

    parser.add_argument(
        '--weather-scaler-name',
        required=True,
        type=str,
        dest='weather_scaler_name',
        help='the scaler used for scaling the data (minmax or standard).',
    )

    parser.add_argument(
        '--number-of-folds',
        required=True,
        type=int,
        dest='number_of_folds',
        help='the number of folds for K-Fold cross validation.',
    )

    parser.add_argument(
        '--number-of-neurons',
        required=True,
        type=int,
        dest='number_of_neurons',
        help='the number of neurons.',
    )

    parser.add_argument(
        '--number-of-epochs',
        required=True,
        type=int,
        dest='number_of_epochs',
        help='the number of epochs.',
    )

    parser.add_argument(
        '--loss-function-name',
        dest='loss_function_name',
        type=str,
        default='mean_squared_error',
        help='the name of the loss function (mean_squared_error).',
    )

    parser.add_argument(
        '--optimizer-name',
        type=str,
        dest='optimizer_name',
        default='adam',
        help='the name of the optimizer (adam).',
    )

    parser.add_argument(
        '--save-data',
        dest='save_data',
        action='store_true',
        help='if the data used for the experience should be saved.',
    )

    parser.add_argument(
        '--data-output-directory',
        dest='data_output_directory',
        type=str,
        default=None,
        help='where the data used for the experience will be saved.',
    )

    parser.add_argument(
        '--make-experience',
        dest='make_experience',
        action='store_true',
        help='if the experience should be done.',
    )

    parser.add_argument(
        '--save-training-progress',
        dest='save_training_progress',
        action='store_true',
        help='if the training-progress of the experience should be saved.',
    )

    parser.add_argument(
        '--training-progress-output-directory',
        dest='training_progress_output_directory',
        type=str,
        default=None,
        help='where the training-progress of the experience will be saved.',
    )

    parser.add_argument(
        '--save-model',
        dest='save_model',
        action='store_true',
        help='if the model of the experience should be saved.',
    )

    parser.add_argument(
        '--models-output-directory',
        dest='models_output_directory',
        type=str,
        default=None,
        help='where the models of the experience will be saved.',
    )

    parser.add_argument(
        '--save-predictions',
        dest='save_predictions',
        action='store_true',
        help='if the predictions from the model should be saved.',
    )

    parser.add_argument(
        '--predictions-output-directory',
        dest='predictions_output_directory',
        type=str,
        default=None,
        help='where the predictions from the model will be saved.',
    )

    args = parser.parse_args()

    if args.save_data is True \
       and args.data_output_directory is None:
        raise Exception((
            '--save-data specified '
            'but not --data-output-directory'
        ))

    if args.save_training_progress is True \
       and args.training_progress_output_directory is None:
        raise Exception((
            '--save-training-progress specified '
            'but not --training-progress-output-directory'
        ))

    if args.save_model is True \
       and args.models_output_directory is None:
        raise Exception((
            '--save-model specified '
            'but not --models-output-directory'
        ))

    if args.save_predictions is True \
       and args.predictions_output_directory is None:
        raise Exception((
            '--save-predictions specified '
            'but not --predictions-output-directory'
        ))

    return args
"""parse_args_begin"""

"""import_crop_dataset_begin"""
def import_crop_dataset(
    crops_file,
    cols_names,
    cols_types,
    separator,
):
    crop_data = pd.read_csv(
        crops_file,
        sep=separator,
        names=cols_names,
        dtype=cols_types,
        index_col='year',
        na_values=['-'],
        header=0,
    )

    return crop_data
"""import_crop_dataset_end"""

"""clean_crop_dataset_begin"""
def clean_crop_dataset(
    crop_data,
    crop_to_experiment,
):
    # Only keep the crop to experiment
    crop_data = crop_data[[crop_to_experiment]].copy()

    # Rename the column to 'yield'
    crop_data.rename(
        columns={crop_to_experiment: 'yield'},
        inplace=True,
    )

    # Only keep years with yield data
    crop_data.dropna(inplace=True)

    return crop_data
"""clean_crop_dataset_end"""

"""create_crop_seasons_dataset_begin"""
def create_crop_seasons_dataset(
    crop_data,
    crop_sowing_day,
    crop_sowing_month,
    crop_harvesting_day,
    crop_harvesting_month,
    crop_ripeness_time,
    weather_resample_by,
):
    crop_seasons = np.empty(
        (crop_data.size, 2),
        dtype='datetime64[D]',
    )

    crop_season = 0

    # Create the crop seasons
    for year, row in crop_data.itertuples():
        sowing_date = dt.datetime(
            year - crop_ripeness_time,
            crop_sowing_month,
            crop_sowing_day,
            0,
            0,
            0,
        )

        harvesting_date = dt.datetime(
            year,
            crop_harvesting_month,
            crop_harvesting_day,
            23,
            59,
            59,
        )

        crop_seasons[crop_season][0] = sowing_date
        crop_seasons[crop_season][1] = harvesting_date

        crop_season += 1

    # Transform the numpy array to pandas DataFrame
    crop_seasons = pd.DataFrame(crop_seasons, columns=[
        'sowing_date',
        'harvesting_date'
    ])

    # Get the yield
    crop_seasons['yield'] = crop_data['yield'].tolist()

    return crop_seasons
"""create_crop_seasons_dataset_end"""

"""import_weather_dataset_begin"""
def import_weather_dataset(
    weather_file,
    cols_names,
    cols_types,
    separator,
    weather_variables,
):
    weather_data = pd.read_csv(
        weather_file,
        sep=separator,
        names=cols_names,
        dtype=cols_types,
        index_col='time',
        parse_dates=['time'],
        usecols=['time'] + weather_variables,
        header=0,
    )

    return weather_data
"""import_weather_dataset_end"""

"""split_by_crop_season_begin"""
def split_by_crop_season(
    weather_data,
    crop_seasons,
):
    weather_data_by_crop_season = []

    for crop_season in crop_seasons.itertuples():
        sowing_date = crop_season.sowing_date
        harvesting_date = crop_season.harvesting_date

        sample_data = weather_data.loc[sowing_date:harvesting_date]

        weather_data_by_crop_season.append(sample_data)

    return weather_data_by_crop_season
"""split_by_crop_season_end"""

"""split_weather_season_by_begin"""
def split_weather_season_by(
    weather_data_by_crop_season,
    split_by,
):
    weather_seasons = []

    for crop_season in weather_data_by_crop_season:
        splited = [g for n, g in crop_season.groupby(
            pd.Grouper(freq=split_by)
        )]

        weather_seasons.append(splited)

    return weather_seasons
"""split_weather_season_by_end"""

"""resample_begin"""
def resample(
    weather_data_by_crop_season,
    resample_by,
    fn,
):
    resampled_weather_data_by_crop_season = []
    cleaned_weather_data_by_crop_season = []

    common_indexes = None
    strftime_format = None

    # Set the format for datetime convertion to string
    if (resample_by == 'H'):
        strftime_format = '%m-%d %H:%M:%S'
    elif (resample_by == 'D'):
        strftime_format = '%m-%d'
    elif (resample_by == 'W'):
        strftime_format = '%W'
    elif (resample_by == 'M'):
        strftime_format = '%m'
    elif (resample_by == 'Y'):
        strftime_format = '%Y'
    else:
        strftime_format = '%m-%d %H:%M:%S'

    # Resample the weather data
    for weather_data in weather_data_by_crop_season:
        resampled_data = weather_data.resample(resample_by).apply(fn)

        # Format the indexes
        resampled_data.index = resampled_data.index.strftime(
            strftime_format
        )

        resampled_weather_data_by_crop_season.append(resampled_data)

    # Find all the common indexes in the crop seasons
    for weather_data in resampled_weather_data_by_crop_season:
        indexes = weather_data.index

        if (common_indexes is not None):
            common_indexes = indexes.intersection(common_indexes)
        else:
            common_indexes = indexes

    # Keep only the common indexes from the crop seasons
    for weather_data in resampled_weather_data_by_crop_season:
        weather_data = weather_data.loc[common_indexes]

        cleaned_weather_data_by_crop_season.append(weather_data)

    return cleaned_weather_data_by_crop_season, common_indexes
"""resample_end"""

"""transform_to_stationary_data_begin"""
def transform_to_stationary_data(
    weather_data_by_crop_season,
):
    stationary_weather_data_by_crop_season = []

    for weather_data in weather_data_by_crop_season:
        stationary_data = weather_data.diff()
        stationary_data.fillna(0, inplace=True)

        stationary_data.index = weather_data.index

        stationary_weather_data_by_crop_season.append(stationary_data)

    return stationary_weather_data_by_crop_season
"""transform_to_stationary_data_end"""

"""scale_begin"""
def scale(
    weather_data_by_crop_season,
    scaler,
):
    # Concat the crop seasons
    weather_data = pd.concat(weather_data_by_crop_season)

    # Fit the scaler on all the data
    scaler.fit(weather_data)

    scaled_weather_data_by_crop_season = []

    # Apply the scaler on each crop season
    for weather_data in weather_data_by_crop_season:
        scaled_data = pd.DataFrame(
            scaler.transform(weather_data),
            columns=weather_data.columns,
        )

        # Put back the indexes
        scaled_data.index = weather_data.index

        scaled_weather_data_by_crop_season.append(scaled_data)

    return scaled_weather_data_by_crop_season
"""scale_end"""

"""split_by_folds_begin"""
def split_by_folds(
    features,
    labels,
    number_of_folds,
    scaler_fn,
):
    k_fold = KFold(n_splits=number_of_folds)

    train_datasets = []
    test_datasets = []

    for train_indexes, test_indexes in k_fold.split(features, labels):
        X_train = features[train_indexes[0]:train_indexes[-1] + 1]
        X_test = features[test_indexes[0]:test_indexes[-1] + 1]

        # Scale features datasets independently
        X_train = scale(X_train, scaler_fn)
        X_test = scale(X_test, scaler_fn)

        y_train = labels[train_indexes[0]:train_indexes[-1] + 1]
        y_test = labels[test_indexes[0]:test_indexes[-1] + 1]

        train_datasets.append({'X': X_train, 'y': y_train})
        test_datasets.append({'X': X_test, 'y': y_test})

    return train_datasets, test_datasets
"""split_by_folds_end"""

"""to_features_data_begin"""
def to_features_data(
    dataset,
):
    X_seq = []

    for sample in dataset:
        X = sample.values

        X_seq.append(X)

    X_seq = np.array(X_seq)

    return X_seq
"""to_features_data_end"""

"""to_labels_data_begin"""
def to_labels_data(
    dataset,
):
    y_seq = []

    for sample in dataset:
        y_seq.append([sample])

    y_seq = np.array(y_seq)

    return y_seq
"""to_labels_data_end"""

# Script
args = parse_args()

crop_to_experiment = args.crop_to_experiment
crop_sowing_month = args.crop_sowing_month
crop_sowing_day = args.crop_sowing_day
crop_harvesting_month = args.crop_harvesting_month
crop_harvesting_day = args.crop_harvesting_day
crop_ripeness_time = args.crop_ripeness_time
weather_variables = args.weather_variables
weather_resample_by = args.weather_resample_by
weather_resample_fn_name = args.weather_resample_fn_name
weather_scaler_name = args.weather_scaler_name
number_of_folds = args.number_of_folds
number_of_neurons = args.number_of_neurons
number_of_epochs = args.number_of_epochs
loss_function_name = args.loss_function_name
optimizer_name = args.optimizer_name
save_data = args.save_data
data_output_directory = args.data_output_directory
make_experience = args.make_experience
save_training_progress = args.save_training_progress
training_progress_output_directory = args.training_progress_output_directory
save_model = args.save_model
models_output_directory = args.models_output_directory
save_predictions = args.save_predictions
predictions_output_directory = args.predictions_output_directory

formatted_crop_to_experiment = crop_to_experiment.replace('_', '-')
formatted_weather_resample_fn = weather_resample_fn_name.replace('_', '-')
formatted_weather_scaler = weather_scaler_name.replace('_', '-')
formatted_loss_function = loss_function_name.replace('_', '-')
formatted_optimizer = optimizer_name.replace('_', '-')

if (weather_resample_fn_name == 'min'):
    weather_resample_fn = pd.DataFrame.min
elif (weather_resample_fn_name == 'std'):
    weather_resample_fn = pd.DataFrame.std
elif (weather_resample_fn_name == 'mean'):
    weather_resample_fn = pd.DataFrame.mean
elif (weather_resample_fn_name == 'max'):
    weather_resample_fn = pd.DataFrame.max

if (weather_scaler_name == 'minmax'):
    scaler_fn = MinMaxScaler(feature_range=(-1, 1))
elif (weather_scaler_name == 'standard'):
    scaler_fn = StandardScaler()

print(DISPLAY_TEMPLATE.format(
    crop_to_experiment,
    number_of_neurons,
    number_of_epochs,
    loss_function_name,
    optimizer_name,
    weather_resample_by,
    weather_resample_fn_name,
    weather_scaler_name,
))

crop_data = import_crop_dataset(
    CROPS_FILE,
    CROPS_FILE_COLS_NAMES,
    CROPS_FILE_COLS_TYPES,
    CSV_SEPARATOR,
)

cleaned_crop_data = clean_crop_dataset(
    crop_data,
    crop_to_experiment,
)

weather_data = import_weather_dataset(
    WEATHER_FILE,
    WEATHER_FILE_COLS_NAMES,
    WEATHER_FILE_COLS_TYPES,
    CSV_SEPARATOR,
    weather_variables,
)

crop_seasons = create_crop_seasons_dataset(
    cleaned_crop_data,
    crop_sowing_day,
    crop_sowing_month,
    crop_harvesting_day,
    crop_harvesting_month,
    crop_ripeness_time,
    weather_resample_by,
)

weather_data_by_crop_season = split_by_crop_season(
    weather_data,
    crop_seasons,
)

resampled_weather_data_by_crop_season, common_indexes = resample(
    weather_data_by_crop_season,
    weather_resample_by,
    weather_resample_fn,
)

stationary_weather_data_by_crop_season = transform_to_stationary_data(
    resampled_weather_data_by_crop_season
)

number_of_samples = len(stationary_weather_data_by_crop_season)
number_of_timesteps = len(common_indexes)
number_of_features = len(weather_variables)

X = stationary_weather_data_by_crop_season
y = crop_seasons['yield'].values

train_datasets, test_datasets = split_by_folds(
    X,
    y,
    number_of_folds,
    scaler_fn,
)

if (make_experience):
    for fold in range(number_of_folds):
        filename = FILES_TEMPLATE.format(
            number_of_neurons,
            number_of_epochs,
            formatted_loss_function,
            formatted_optimizer,
            fold,
            weather_resample_by,
            weather_resample_fn_name,
            formatted_weather_scaler,
        )

        """lstm_model_begin"""
        model = Sequential()

        model.add(GRU(
            number_of_neurons,
            input_shape=(number_of_timesteps, number_of_features),
            return_sequences=False,
        ))

        model.add(Dense(1))

        model.compile(
            loss=loss_function_name,
            optimizer=optimizer_name,
        )
        """lstm_model_end"""

        """lstm_train_begin"""
        X_train = to_features_data(train_datasets[fold]['X'])
        y_train = to_labels_data(train_datasets[fold]['y'])

        history = model.fit(
            X_train,
            y_train,
            epochs=number_of_epochs,
            shuffle=False,
            verbose=2,
        )
        """lstm_train_end"""

        history = utils.to_pandas_df(history.history, ['loss'])

        """lstm_predict_begin"""
        X_test = to_features_data(test_datasets[fold]['X'])
        y_test = to_labels_data(test_datasets[fold]['y'])

        predictions = model.predict(X_test)
        """lstm_predict_end"""

        predictions = predictions.reshape(predictions.shape[0])
        predictions = utils.to_pandas_df(predictions, ['predictions'])

        expectations = utils.to_pandas_df(y_test, ['expectations'])

        results = pd.concat([expectations, predictions], axis=1)

        print(results)

        if (save_training_progress):
            utils.save_df(
                history,
                '{}/{}'.format(
                    training_progress_output_directory,
                    formatted_crop_to_experiment,
                ),
                filename,
            )

        if (save_model):
            utils.save_model(
                model,
                '{}/{}'.format(
                    models_output_directory,
                    formatted_crop_to_experiment,
                ),
                MODELS_TEMPLATE.format(
                    number_of_neurons,
                    number_of_epochs,
                    formatted_loss_function,
                    formatted_optimizer,
                    weather_resample_by,
                    weather_resample_fn_name,
                    formatted_weather_scaler,
                ),
            )

        if (save_predictions):
            utils.save_df(
                results,
                '{}/{}'.format(
                    predictions_output_directory,
                    formatted_crop_to_experiment,
                ),
                filename,
            )

if (save_data):
    years = cleaned_crop_data.index

    scaled_weather_data_by_crop_season = scale(
        stationary_weather_data_by_crop_season,
        scaler_fn,
    )

    scatted_weather_data_by_crop_season = utils.to_scatter(
        scaled_weather_data_by_crop_season,
    )

    dfs_output = '{}/{}/by-{}/{}-fn'.format(
        data_output_directory,
        formatted_crop_to_experiment,
        weather_resample_by,
        weather_resample_fn_name,
    )

    utils.save_df(
        crop_data,
        data_output_directory,
        'yield-average-per-crop',
        header=True,
        csv_separator=CSV_SEPARATOR,
    )

    utils.save_df(
        crop_seasons,
        '{}/{}'.format(
            data_output_directory,
            formatted_crop_to_experiment,
        ),
        'crop-seasons',
        header=True,
        csv_separator=CSV_SEPARATOR,
    )

    utils.save_df(
        weather_data,
        data_output_directory,
        'weather-dataset',
        header=True,
        csv_separator=CSV_SEPARATOR,
    )

    utils.save_dfs(
        weather_data_by_crop_season,
        '{}/{}'.format(
            data_output_directory,
            formatted_crop_to_experiment,
        ),
        years,
        'weather-data',
        header=True,
        csv_separator=CSV_SEPARATOR,
    )

    utils.save_dfs(
        resampled_weather_data_by_crop_season,
        dfs_output,
        years,
        'resampled-weather-data',
        header=True,
        csv_separator=CSV_SEPARATOR,
    )

    utils.save_dfs(
        stationary_weather_data_by_crop_season,
        dfs_output,
        years,
        'stationary-weather-data',
        header=True,
        csv_separator=CSV_SEPARATOR,
    )

    utils.save_dfs(
        scaled_weather_data_by_crop_season,
        dfs_output,
        years,
        'scaled-weather-data-{}-scaler'.format(
            weather_scaler_name,
        ),
        header=True,
        csv_separator=CSV_SEPARATOR,
    )

    utils.save_dfs(
        scatted_weather_data_by_crop_season,
        dfs_output,
        years,
        'scattered-weather-data',
        header=['value'],
        csv_separator=CSV_SEPARATOR,
    )
