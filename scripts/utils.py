import os

import pandas as pd

"""to_pandas_df_begin"""
def to_pandas_df(
    array,
    columns,
):
    return pd.DataFrame(array, columns=columns)
"""to_pandas_df_end"""

"""plot_df_begin"""
def plot_df(
    df,
    subplots=True,
):
    df.plot(subplots=subplots)
    input('Press Enter to close plot.')
"""plot_df_end"""

"""save_df_begin"""
def save_df(
    df,
    csv_output_directory,
    filename,
    header=True,
    csv_separator=';',
):
    if not os.path.exists(csv_output_directory):
        os.makedirs(csv_output_directory)

    output = '{}/{}.csv'.format(csv_output_directory, filename)

    df.to_csv(
        output,
        sep=csv_separator,
        encoding='utf-8',
        na_rep='NaN',
        header=header,
    )
"""save_df_end"""

"""save_dfs_begin"""
def save_dfs(
    dfs,
    csv_output_directory,
    years,
    filenames,
    header=True,
    csv_separator=';',
):
    year_index = 0

    for df in dfs:
        year = years[year_index]

        filename = '{}-year-{}'.format(
            filenames,
            year,
        )

        save_df(
            df,
            csv_output_directory,
            filename,
            header,
            csv_separator,
        )

        year_index += 1
"""save_dfs_end"""

"""save_model_begin"""
def save_model(
    model,
    models_output_directory,
    filename,
):
    if not os.path.exists(models_output_directory):
        os.makedirs(models_output_directory)

    output = '{}/{}.hdf5'.format(models_output_directory, filename)

    model.save(output)

"""save_model_end"""

"""to_scatter_begin"""
def to_scatter(
    weather_data_by_crop_season,
):
    scattered_data = []

    for weather_data in weather_data_by_crop_season:
        correlation_matrix = weather_data.corr()
        stacked_matrix = correlation_matrix.stack()

        scattered_data.append(stacked_matrix)

    return scattered_data
"""to_scatter_end"""
