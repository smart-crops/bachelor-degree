# Scripts

## Requirements

- [`git`](https://git-scm.com/)
- [`python3`](https://python.org)

## Create the environment

```sh
# Clone the repository
git clone https://gitlab.com/smart-crops/bachelor-degree.git

# Move to the cloned directory
cd bachelor-degree/scripts

# Create the virtual environment
python3 -m venv .venv

# Source the virtual environment
source .venv/bin/activate

# Install the dependencies
pip3 install -r requirements.txt

# Start the desired script
python3 <a script>.py
```
