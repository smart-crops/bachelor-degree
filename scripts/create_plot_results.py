import glob

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from pylab import ion

import utils

from const import SAVE_TRAINING_PROGRESS
from const import SAVE_PREDICTIONS
from const import SHOW_PLOTS
from const import TRAINING_PROGRESS_DIRECTORY
from const import PREDICTIONS_DIRECTORY
from const import PLOTS_DIRECTORY
from const import CROPS
from const import WEATHER_RESAMPLES
from const import WEATHER_RESAMPLES_FUNCTIONS_NAMES
from const import WEATHER_SCALERS_NAMES
from const import NUMBER_OF_NEURONS
from const import NUMBER_OF_EPOCHS
from const import LOSS_FUNCTIONS_NAMES
from const import OPTIMIZER_FUNCTIONS_NAMES
from const import CSV_SEPARATOR
from const import DISPLAY_TEMPLATE
from const import FILES_TEMPLATE
from const import TRAINING_PROGRESS_FILES_TEMPLATE
from const import PREDICTIONS_FILES_TEMPLATE

ion()

# Functions
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)

    mape = np.mean(np.abs((y_true - y_pred) / y_true)) * 100

    return mape

for crop in \
        CROPS:
    for weather_resample in \
            WEATHER_RESAMPLES:
        for weather_resample_function in \
                WEATHER_RESAMPLES_FUNCTIONS_NAMES:
            output_directory = '{}/{}/by-{}/{}-fn'.format(
                PLOTS_DIRECTORY,
                crop['formatted_name'],
                weather_resample,
                weather_resample_function,
            )

            predictions_summary = []

            for weather_scaler in \
                    WEATHER_SCALERS_NAMES:
                for neurons in \
                        NUMBER_OF_NEURONS:
                    for epochs in \
                            NUMBER_OF_EPOCHS:
                        for loss_function in \
                                LOSS_FUNCTIONS_NAMES:
                            for optimizer_function in \
                                    OPTIMIZER_FUNCTIONS_NAMES:
                                print(DISPLAY_TEMPLATE.format(
                                    crop['formatted_name'],
                                    neurons,
                                    epochs,
                                    loss_function['formatted_name'],
                                    optimizer_function,
                                    weather_resample,
                                    weather_resample_function,
                                    weather_scaler
                                ))

                                input_filenames = FILES_TEMPLATE.format(
                                    neurons,
                                    epochs,
                                    loss_function['formatted_name'],
                                    optimizer_function,
                                    '*',
                                    weather_resample,
                                    weather_resample_function,
                                    weather_scaler,
                                )

                                training_progress_filenames = \
                                    TRAINING_PROGRESS_FILES_TEMPLATE.format(
                                        neurons,
                                        epochs,
                                        loss_function['formatted_name'],
                                        optimizer_function,
                                        weather_scaler,
                                    )

                                predictions_filenames = \
                                    PREDICTIONS_FILES_TEMPLATE.format(
                                        neurons,
                                        epochs,
                                        loss_function['formatted_name'],
                                        optimizer_function,
                                        weather_scaler,
                                    )

                                training_progress_files = glob.glob(
                                    '{}/{}/{}.csv'.format(
                                        TRAINING_PROGRESS_DIRECTORY,
                                        crop['formatted_name'],
                                        input_filenames,
                                    )
                                )

                                predictions_files = glob.glob(
                                    '{}/{}/{}.csv'.format(
                                        PREDICTIONS_DIRECTORY,
                                        crop['formatted_name'],
                                        input_filenames,
                                    )
                                )

                                training_progress_files.sort()
                                predictions_files.sort()

                                # Create the training progress dataset
                                training_progress = []

                                fold = 0
                                for training_progress_file in \
                                        training_progress_files:

                                    fold_formatted = 'fold_{}'.format(fold)

                                    df = pd.read_csv(
                                        training_progress_file,
                                        sep=CSV_SEPARATOR,
                                        names=[
                                            'epoch',
                                            fold_formatted,
                                        ],
                                        usecols=[fold_formatted],
                                        header=0,
                                    )

                                    training_progress.append(df)

                                    fold += 1

                                training_progress = pd.concat(
                                    training_progress,
                                    axis=1,
                                )

                                # Create the predictions dataset
                                predictions = []

                                for predictions_file in \
                                        predictions_files:
                                    df = pd.read_csv(
                                        predictions_file,
                                        sep=CSV_SEPARATOR,
                                        names=[
                                            'index',
                                            'expectations',
                                            'predictions',
                                        ],
                                        usecols=[
                                            'expectations',
                                            'predictions',
                                        ],
                                        header=0,
                                    )

                                    predictions.append(df)

                                predictions = pd.concat(predictions)
                                predictions.reset_index(
                                    inplace=True,
                                    drop=True,
                                )

                                correlation = predictions.corr(
                                    method='pearson',
                                )
                                correlation = correlation.values[0][1]
                                correlation = round(correlation, 2)

                                mape = mean_absolute_percentage_error(
                                    predictions['expectations'],
                                    predictions['predictions'],
                                )

                                '''
                                errors = pd.DataFrame()

                                errors['difference'] = abs(
                                    predictions['predictions'] -
                                    predictions['expectations']
                                )

                                errors['average'] = predictions[[
                                    'predictions',
                                    'expectations',
                                ]].mean(axis=1)

                                errors['mape'] = 100 * \
                                    errors['difference'] / \
                                    errors['average']

                                average_error_percentage = errors[
                                    'error_percentage'
                                ].mean()
                                '''

                                mape = round(mape, 2)

                                predictions_summary.append([
                                    epochs,
                                    neurons,
                                    weather_scaler,
                                    correlation,
                                    mape,
                                ])

                                if (SAVE_TRAINING_PROGRESS):
                                    utils.save_df(
                                        training_progress,
                                        output_directory,
                                        training_progress_filenames,
                                    )

                                if (SAVE_PREDICTIONS):
                                    utils.save_df(
                                        predictions,
                                        output_directory,
                                        predictions_filenames,
                                    )

                                if (SHOW_PLOTS):
                                    # Plot training progress
                                    fig_training_progress, \
                                        ax_training_progress = plt.subplots()

                                    training_progress.plot(
                                        color=['b'],
                                        ax=ax_training_progress,
                                    )

                                    # Plot predictions
                                    fig_predictions, \
                                        ax_predictions = plt.subplots()

                                    ax_predictions.set(
                                        xlim=(-20, 80),
                                        ylim=(-20, 80),
                                    )

                                    ax_predictions.plot(
                                        ax_predictions.get_xlim(),
                                        ax_predictions.get_ylim(),
                                        ls="--",
                                        c=".3",
                                    )

                                    predictions.plot.scatter(
                                        x='expectations',
                                        y='predictions',
                                        ax=ax_predictions,
                                    )

                                    input('Press Enter to close the plot.')

                                    plt.close(fig_training_progress)
                                    plt.close(fig_predictions)

            predictions_summary_cols = [
                'epochs',
                'neurons',
                'scaler',
                'r_value',
                'mape',
            ]

            predictions_summary_order_by = [
                'epochs',
                'neurons',
                'scaler',
                'mape',
                'r_value',
            ]

            predictions_summary = pd.DataFrame(
                predictions_summary,
                columns=predictions_summary_cols,
            )

            predictions_summary.sort_values(
                by=predictions_summary_order_by,
                inplace=True,
            )

            predictions_summary.reset_index(
                drop=True,
                inplace=True,
            )

            if (SAVE_PREDICTIONS):
                utils.save_df(
                    predictions_summary,
                    output_directory,
                    'predictions-summary',
                )
