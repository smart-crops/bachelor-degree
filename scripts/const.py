SAVE_DATA = True
MAKE_EXPERIENCE = True
SAVE_TRAINING_PROGRESS = True
SAVE_MODEL = True
SAVE_PREDICTIONS = True
SHOW_PLOTS = False

DATA_DIRECTORY = '../data/models-data'
TRAINING_PROGRESS_DIRECTORY = '../data/training-progress'
MODELS_DIRECTORY = '../data/models'
PREDICTIONS_DIRECTORY = '../data/predictions'
PLOTS_DIRECTORY = '../data/plots'

CROPS = [
    {
        'name': 'winter_wheat',
        'formatted_name': 'winter-wheat',
        'short_name': 'ww',
    },
    {
        'name': 'winter_field_beans',
        'formatted_name': 'winter-field-beans',
        'short_name': 'wfb',
    },
]

CROPS_FILE = '../data/crops/cleaned/2000-2017-yields-average-per-crop.csv'

CROPS_FILE_COLS_NAMES = [
    'year',
    'winter_wheat',
    'winter_field_beans',
]

CROPS_FILE_COLS_TYPES = {
    'year': str,
    'winter_wheat': float,
    'winter_field_beans': float,
}

WEATHER_FILE = '../data/weather/cleaned/order_72782_data.csv'

WEATHER_FILE_COLS_NAMES = [
    'time',
    'radiation',
    'air_temperature',
    'precipitation',
    'humidity',
]

WEATHER_FILE_COLS_TYPES = {
    'time': str,
    'radiation': float,
    'air_temperature': float,
    'precipitation': float,
    'humidity': float,
}

CSV_SEPARATOR = ';'

DISPLAY_TEMPLATE = (
    'Crop: {}\n'
    '\tNumber of neurons: {}\n'
    '\tNumber of epochs: {}\n'
    '\tLoss fn: {}\n'
    '\tOptimizer: {}\n'
    '\tWeather resample by: {}\n'
    '\tWeather resample fn: {}\n'
    '\tWeather scaler: {}\n'
)

FILES_TEMPLATE = (
    '{}-neurons-'
    '{}-epochs-'
    '{}-loss-function-'
    '{}-optimizer-'
    'fold-{}-'
    'weather-resampled-by-{}-'
    '{}-fn-'
    '{}-scaler'
)

MODELS_TEMPLATE = (
    '{}-neurons-'
    '{}-epochs-'
    '{}-loss-function-'
    '{}-optimizer-'
    'weather-resampled-by-{}-'
    '{}-fn-'
    '{}-scaler'
)

TRAINING_PROGRESS_FILES_TEMPLATE = (
    'training-progress-'
    '{}-neurons-'
    '{}-epochs-'
    '{}-loss-function-'
    '{}-optimizer-'
    '{}-scaler'
)

PREDICTIONS_FILES_TEMPLATE = (
    'predictions-'
    '{}-neurons-'
    '{}-epochs-'
    '{}-loss-function-'
    '{}-optimizer-'
    '{}-scaler'
)

WEATHER_RESAMPLES = ['M', 'W']

WEATHER_RESAMPLES_FUNCTIONS_NAMES = ['mean']

WEATHER_SCALERS_NAMES = ['minmax', 'standard']

NUMBER_OF_FOLDS = 5

NUMBER_OF_NEURONS = [2, 4, 8, 16, 32, 64]

NUMBER_OF_EPOCHS = [500, 1000, 1500, 2000, 3000, 4500]

LOSS_FUNCTIONS_NAMES = [
    {
        'name': 'mean_squared_error',
        'formatted_name': 'mean-squared-error',
    },
]

OPTIMIZER_FUNCTIONS_NAMES = ['adam']
