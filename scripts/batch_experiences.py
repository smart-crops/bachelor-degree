import concurrent.futures
import subprocess

from const import SAVE_DATA
from const import MAKE_EXPERIENCE
from const import SAVE_TRAINING_PROGRESS
from const import SAVE_MODEL
from const import SAVE_PREDICTIONS
from const import DATA_DIRECTORY
from const import TRAINING_PROGRESS_DIRECTORY
from const import MODELS_DIRECTORY
from const import PREDICTIONS_DIRECTORY
from const import CROPS
from const import WEATHER_RESAMPLES
from const import WEATHER_RESAMPLES_FUNCTIONS_NAMES
from const import WEATHER_SCALERS_NAMES
from const import NUMBER_OF_FOLDS
from const import NUMBER_OF_NEURONS
from const import NUMBER_OF_EPOCHS
from const import LOSS_FUNCTIONS_NAMES
from const import OPTIMIZER_FUNCTIONS_NAMES

COMMAND_TEMPLATE = (
    'python3 {} '
    '--number-of-neurons={} '
    '--number-of-epochs={} '
    '--loss-function-name={} '
    '--optimizer-name={} '
    '--number-of-folds={} '
    '--weather-resample-by={} '
    '--weather-resample-fn-name={} '
    '--weather-scaler-name={} '
    '{} '
    '--data-output-directory={} '
    '{} '
    '{} '
    '--training-progress-output-directory={} '
    '{} '
    '--models-output-directory={} '
    '{} '
    '--predictions-output-directory={} '
)

# Functions
def execute(command):
    subprocess.call(command, shell=True)

# Script
commands = []

if (SAVE_DATA):
    save_data = '--save-data'
else:
    save_data = ''

if (MAKE_EXPERIENCE):
    make_experience = '--make-experience'
else:
    make_experience = ''

if (SAVE_TRAINING_PROGRESS):
    save_training_progress = '--save-training-progress'
else:
    save_training_progress = ''

if (SAVE_MODEL):
    save_model = '--save-model'
else:
    save_model = ''

if (SAVE_PREDICTIONS):
    save_predictions = '--save-predictions'
else:
    save_predictions = ''

for crop in \
        CROPS:
    for weather_resample in \
            WEATHER_RESAMPLES:
        for weather_resample_function in \
                WEATHER_RESAMPLES_FUNCTIONS_NAMES:
            for weather_scaler in \
                    WEATHER_SCALERS_NAMES:
                for neurons in \
                        NUMBER_OF_NEURONS:
                    for epochs in \
                            NUMBER_OF_EPOCHS:
                        for loss_function in \
                                LOSS_FUNCTIONS_NAMES:
                            for optimizer_function in \
                                    OPTIMIZER_FUNCTIONS_NAMES:
                                script = '{}-experience.py'.format(
                                    crop['short_name'],
                                )

                                command = COMMAND_TEMPLATE.format(
                                    script,
                                    neurons,
                                    epochs,
                                    loss_function['name'],
                                    optimizer_function,
                                    NUMBER_OF_FOLDS,
                                    weather_resample,
                                    weather_resample_function,
                                    weather_scaler,
                                    save_data,
                                    DATA_DIRECTORY,
                                    make_experience,
                                    save_training_progress,
                                    TRAINING_PROGRESS_DIRECTORY,
                                    save_model,
                                    MODELS_DIRECTORY,
                                    save_predictions,
                                    PREDICTIONS_DIRECTORY,
                                )

                                commands.append(command)

with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
    executor.map(execute, commands)
