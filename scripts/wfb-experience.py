import argparse
import subprocess

# Consts
"""crop_details_begin"""
CROP_TO_EXPERIMENT = 'winter_field_beans'
CROP_SOWING_MONTH = 10
CROP_SOWING_DAY = 15
CROP_HARVESTING_MONTH = 8
CROP_HARVESTING_DAY = 15
CROP_RIPENESS_TIME = 1
WEATHER_VARIABLES = [
    'radiation',
    'air_temperature',
    'humidity',
]
"""crop_details_end"""

COMMAND_TEMPLATE = (
    'python3 experience.py '
    '--crop-to-experiment={} '
    '--crop-sowing-month={} '
    '--crop-sowing-day={} '
    '--crop-harvesting-month={} '
    '--crop-harvesting-day={} '
    '--crop-ripeness-time={} '
    '--weather-variables "{}" '
    '--number-of-neurons={} '
    '--number-of-epochs={} '
    '--loss-function-name={} '
    '--optimizer-name={} '
    '--number-of-folds={} '
    '--weather-resample-by={} '
    '--weather-resample-fn-name={} '
    '--weather-scaler-name={} '
    '{} '
    '--data-output-directory={} '
    '{} '
    '{} '
    '--training-progress-output-directory={} '
    '{} '
    '--models-output-directory={} '
    '{} '
    '--predictions-output-directory={} '
)

# Functions
"""parse_args_begin"""
def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--weather-resample-by',
        required=True,
        type=str,
        dest='weather_resample_by',
        help='how to resample the weather data (H, D, W, M or Y).',
    )

    parser.add_argument(
        '--weather-resample-fn-name',
        required=True,
        type=str,
        dest='weather_resample_fn_name',
        help='the resampling function to use (min, std, mean or max).',
    )

    parser.add_argument(
        '--weather-scaler-name',
        required=True,
        type=str,
        dest='weather_scaler_name',
        help='the scaler used for scaling the data (minmax or standard).',
    )

    parser.add_argument(
        '--number-of-folds',
        required=True,
        type=int,
        dest='number_of_folds',
        help='the number of folds for K-Fold cross validation.',
    )

    parser.add_argument(
        '--number-of-neurons',
        required=True,
        type=int,
        dest='number_of_neurons',
        help='the number of neurons.',
    )

    parser.add_argument(
        '--number-of-epochs',
        required=True,
        type=int,
        dest='number_of_epochs',
        help='the number of epochs.',
    )

    parser.add_argument(
        '--loss-function-name',
        dest='loss_function_name',
        type=str,
        default='mean_squared_error',
        help='the name of the loss function (mean_squared_error).',
    )

    parser.add_argument(
        '--optimizer-name',
        type=str,
        dest='optimizer_name',
        default='adam',
        help='the name of the optimizer (adam).',
    )

    parser.add_argument(
        '--save-data',
        dest='save_data',
        action='store_true',
        help='if the data used for the experience should be saved.',
    )

    parser.add_argument(
        '--data-output-directory',
        dest='data_output_directory',
        type=str,
        default=None,
        help='where the data used for the experience will be saved.',
    )

    parser.add_argument(
        '--make-experience',
        dest='make_experience',
        action='store_true',
        help='if the experience should be done.',
    )

    parser.add_argument(
        '--save-training-progress',
        dest='save_training_progress',
        action='store_true',
        help='if the training-progress of the experience should be saved.',
    )

    parser.add_argument(
        '--training-progress-output-directory',
        dest='training_progress_output_directory',
        type=str,
        default=None,
        help='where the training-progress of the experience will be saved.',
    )

    parser.add_argument(
        '--save-model',
        dest='save_model',
        action='store_true',
        help='if the model of the experience should be saved.',
    )

    parser.add_argument(
        '--models-output-directory',
        dest='models_output_directory',
        type=str,
        default=None,
        help='where the models of the experience will be saved.',
    )

    parser.add_argument(
        '--save-predictions',
        dest='save_predictions',
        action='store_true',
        help='if the predictions from the model should be saved.',
    )

    parser.add_argument(
        '--predictions-output-directory',
        dest='predictions_output_directory',
        type=str,
        default=None,
        help='where the predictions from the model will be saved.',
    )

    args = parser.parse_args()

    if args.save_data is True \
       and args.data_output_directory is None:
        raise Exception((
            '--save-data specified '
            'but not --data-output-directory'
        ))

    if args.save_training_progress is True \
       and args.training_progress_output_directory is None:
        raise Exception((
            '--save-training-progress specified '
            'but not --training-progress-output-directory'
        ))

    if args.save_model is True \
       and args.models_output_directory is None:
        raise Exception((
            '--save-model specified '
            'but not --models-output-directory'
        ))

    if args.save_predictions is True \
       and args.predictions_output_directory is None:
        raise Exception((
            '--save-predictions specified '
            'but not --predictions-output-directory'
        ))

    return args
"""parse_args_begin"""

# Script
args = parse_args()

number_of_neurons = args.number_of_neurons
number_of_epochs = args.number_of_epochs
loss_function_name = args.loss_function_name
optimizer_name = args.optimizer_name
number_of_folds = args.number_of_folds
weather_resample_by = args.weather_resample_by
weather_resample_fn_name = args.weather_resample_fn_name
weather_scaler_name = args.weather_scaler_name
save_data = args.save_data
data_output_directory = args.data_output_directory
make_experience = args.make_experience
save_training_progress = args.save_training_progress
training_progress_output_directory = args.training_progress_output_directory
save_model = args.save_model
models_output_directory = args.models_output_directory
save_predictions = args.save_predictions
predictions_output_directory = args.predictions_output_directory

if (save_data):
    save_data = '--save-data'
else:
    save_data = ''

if (make_experience):
    make_experience = '--make-experience'
else:
    make_experience = ''

if (save_training_progress):
    save_training_progress = '--save-training-progress'
else:
    save_training_progress = ''

if (save_model):
    save_model = '--save-model'
else:
    save_model = ''

if (save_predictions):
    save_predictions = '--save-predictions'
else:
    save_predictions = ''

command = COMMAND_TEMPLATE.format(
    CROP_TO_EXPERIMENT,
    CROP_SOWING_MONTH,
    CROP_SOWING_DAY,
    CROP_HARVESTING_MONTH,
    CROP_HARVESTING_DAY,
    CROP_RIPENESS_TIME,
    '" "'.join(WEATHER_VARIABLES),
    number_of_neurons,
    number_of_epochs,
    loss_function_name,
    optimizer_name,
    number_of_folds,
    weather_resample_by,
    weather_resample_fn_name,
    weather_scaler_name,
    save_data,
    data_output_directory,
    make_experience,
    save_training_progress,
    training_progress_output_directory,
    save_model,
    models_output_directory,
    save_predictions,
    predictions_output_directory,
)

# Call the experiment script
subprocess.call(command, shell=True)
