import sys
import time

import numpy as np

from matplotlib.backends.qt_compat import QtCore, QtWidgets, is_pyqt5
if is_pyqt5():
    from matplotlib.backends.backend_qt5agg import (
        FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
else:
    from matplotlib.backends.backend_qt4agg import (
        FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure


class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self._main = QtWidgets.QWidget()
        self.setCentralWidget(self._main)
        layout = QtWidgets.QVBoxLayout(self._main)

        static_canvas = FigureCanvas(Figure(figsize=(5, 3)))
        layout.addWidget(static_canvas)
        self.addToolBar(NavigationToolbar(static_canvas, self))

        dynamic_canvas = FigureCanvas(Figure(figsize=(5, 3)))
        layout.addWidget(dynamic_canvas)
        self.addToolBar(QtCore.Qt.BottomToolBarArea,
                        NavigationToolbar(dynamic_canvas, self))

        self._static_ax = static_canvas.figure.subplots()
        t = np.linspace(0, 10, 501)
        self._static_ax.plot(t, np.tan(t), ".")

        self._dynamic_ax = dynamic_canvas.figure.subplots()
        self._timer = dynamic_canvas.new_timer(
            100, [(self._update_canvas, (), {})])
        self._timer.start()

    def _update_canvas(self):
        self._dynamic_ax.clear()
        t = np.linspace(0, 10, 101)
        # Shift the sinusoid as a function of time.
        self._dynamic_ax.plot(t, np.sin(t + time.time()))
        self._dynamic_ax.figure.canvas.draw()


if __name__ == "__main__":
    qapp = QtWidgets.QApplication(sys.argv)
    app = ApplicationWindow()
    app.show()
    qapp.exec_()

'''
import keras.models as models
import numpy as np
import pandas as pd

from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg,
    NavigationToolbar2Tk
)
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
from tkinter import *

import utils


# Consts
DATA_DIRECTORY = '../data/models-data'
MODELS_DIRECTORY = '../data/models'

# Create the window
window = Tk()
window.title('Smart Crops - Environment viewer')


def import_csv(csv_file, csv_separator=';'):
    df = pd.read_csv(
        csv_file,
        sep=csv_separator,
        index_col=0,
    )

    df.index.name = 'labels'

    df.reset_index(inplace=True)

    return df


def import_model(model_file):
    return models.load_model(model_file)


def load_crop_seasons():
    crop_seasons_file = filedialog.askopenfilename(
        title='Choose the crop season file',
        initialdir=DATA_DIRECTORY,
        filetypes=(
            ('Crop season file', 'crop-seasons.csv'),
            ('All files', '*')
        ),
    )

    return crop_seasons_file


def load_weather_season():
    weather_season_file = tkinter.filedialog.askopenfilename(
        title='Choose the weather season file',
        initialdir=DATA_DIRECTORY,
        filetypes=(
            ('Weather season file', 'resampled-weather-data-year-*.csv'),
            ('All files', '*')
        ),
    )

    return weather_season_file


def load_model():
    model_file = tkinter.filedialog.askopenfilename(
        title='Choose the model file',
        initialdir=MODELS_DIRECTORY,
        filetypes=(
            ('Weather season file', (
                'model-*-training-percentage-*-neurons-'
                '*-epochs-*-loss-function-*-optimizer-'
                'weather-resampled-by-*-*-fn-*-scaler.hdf5'
            )),
            ('All files', '*')
        ),
    )

    return model_file


def load_files():
    crop_season_file = '../data/plots/winter-wheat/crop-seasons.csv' #  load_crop_seasons()
    weather_season_file = '../data/plots/winter-wheat/by-M/mean-fn/resampled-weather-data-year-2000.csv' #  load_weather_season()
    model_file = '../data/models/winter-field-beans/model-0.2-training-percentage-1-neurons-500-epochs-mean-squared-error-loss-function-adam-optimizer-weather-resampled-by-W-mean-fn-minmax-scaler.hdf5' #  load_model()

    crop_season_df = import_csv(crop_season_file)
    weather_season_df = import_csv(weather_season_file)
    #model_keras = import_model(model_file)

    row_index = 0
    for weather_variable in weather_season_df.columns:
        if (weather_variable != 'labels'):
            add_plot_to_grid(
                df=weather_season_df,
                column_to_plot=weather_variable,
                xlabel='timesteps',
                ylabel=weather_variable,
                row=row_index,
                column=0,
            )
            
            add_plot_to_grid(
                df=weather_season_df,
                column_to_plot=weather_variable,
                xlabel='timesteps',
                ylabel=weather_variable,
                row=row_index + 1,
                column=0,
            )

        row_index += 2

    


def add_plot_to_grid(
    df,
    column_to_plot,
    xlabel,
    ylabel,
    row,
    column,
):
    figure = Figure(figsize=figsize, dpi=dpi)

    ax = figure.add_subplot(2, 1, 1)

    ax.set(xlabel=xlabel, ylabel=ylabel)
    ax.set_xticklabels(df.labels)

    df.plot(y=column_to_plot, xticks=df.index, ax=ax)

    canvas = FigureCanvasTkAgg(figure, master=window)

    canvas.draw()

    canvas.get_tk_widget().grid(row=row, column=column, padx=5, pady=5)


def quit():
    window.quit()
    window.destroy()
    exit()

# Create the menu
menu = Menu(window)
window.config(menu=menu)

# Create the main menu
main = Menu(menu)
main.add_command(label='Load files', command=load_files)
main.add_separator()
main.add_command(label='Exit', command=quit)

# Add to the main menu
menu.add_cascade(label='Menu', menu=main)

class WeatherVariableViewer:
    def __init__(
        self,
        parent,
        variable_name,
        title,
        xlabel,
        ylabel,
        df,
        figsize=(5, 2),
        dpi=100,
    ):
        frame = Frame(parent)

        figure = Figure(figsize=figsize, dpi=dpi)

        ax = figure.add_subplot(2, 1, 1)

        ax.set(xlabel=xlabel, ylabel=ylabel)
        ax.set_xticklabels(df.labels)

        df.plot(
            title=title,
            y=variable_name,
            xticks=df.index,
            ax=ax,
        )

        self.canvas = FigureCanvasTkAgg(figure, master=window)

        self.canvas.draw()

        self.canvas.get_tk_widget().pack(side='top', fill='both', expand=1)
        Button(frame, text='TABLE HERE', command=self.refresh).pack()
        Button(frame, text='Refresh', command=self.refresh).pack()

        frame.pack()

    def refresh(self):
        print('Hello frame world')

# Create the grid
weather_season_df = import_csv('../data/plots/winter-wheat/by-M/mean-fn/resampled-weather-data-year-2000.csv')

parent = Frame(None)

parent.pack()

WeatherVariableViewer(
    parent,
    'air_temperature',
    'Air temperature',
    'Weeks number',
    'Temperature [°C]',
    weather_season_df,
)




canvas = FigureCanvasTkAgg(fig, master=window)  # A tk.DrawingArea.
canvas.draw()
canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)

toolbar = NavigationToolbar2Tk(canvas, window)
toolbar.update()
canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)

def on_key_press(event):
    print("you pressed {}".format(event.key))
    key_press_handler(event, canvas, toolbar)


canvas.mpl_connect("key_press_event", on_key_press)


window.mainloop()
'''