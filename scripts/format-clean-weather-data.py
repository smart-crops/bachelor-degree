import pandas as pd


def dateparse(string):
    '''Convert the string to a datetime object from the format.'''
    format = '%Y%m%d%H'
    return pd.datetime.strptime(string, format)

# Consts - change if needed
input_file = '../data/weather/raw/order_72782_data.txt'
output_file = '../data/weather/cleaned/order_72782_data.csv'

columns_name = [
    'station',
    'time',
    'radiation',
    'temperature',
    'precipitation',
    'humidity',
]

columns_to_keep = [
    'time',
    'radiation',
    'temperature',
    'precipitation',
    'humidity',
]

missing_values = ['-']

# Load file
df = pd.read_csv(input_file, sep=';', index_col='time', dtype={
        'time': str,
        'radiation': float,
        'temperature': float,
        'humidity': float,
        'precipitation': float,
    },
    names=columns_name,
    usecols=columns_to_keep,
    skip_blank_lines=True,
    na_values=missing_values,
    parse_dates=['time'],
    date_parser=dateparse,
    header=0)

# Remove negative values from radiation
df.loc[df['radiation'] < 0, 'radiation'] = 0

# Save to CSV file
df.to_csv(output_file, sep=';', encoding='utf-8', na_rep='NaN')
