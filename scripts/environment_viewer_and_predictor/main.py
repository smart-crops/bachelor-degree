import sys

from keras.models import load_model
import numpy as np
import pandas as pd

from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QInputDialog
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QListWidgetItem
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler


from PyQt5 import uic

Ui_MainWindow, QtBaseClass = uic.loadUiType('ui.ui')


class EnvironmentViewerAndPredictor(QMainWindow):
    def __init__(self):
        super(EnvironmentViewerAndPredictor, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.crop_season_file = None
        self.crop = None
        self.year = None
        self.weather_season_file = None
        self.model_file = None
        self.model_details = None

        self.ui.button_select_crop_season_file.clicked.connect(
            self.selectCropSeasonFile
        )

        self.ui.button_select_weather_season_file.clicked.connect(
            self.selectWeatherSeasonFile
        )

        self.ui.button_select_model_file.clicked.connect(
            self.selectModelFile
        )

        self.air_temperature_inputs = [
            (self.ui.at_input_october, self.ui.at_slider_october),
            (self.ui.at_input_november, self.ui.at_slider_november),
            (self.ui.at_input_december, self.ui.at_slider_december),
            (self.ui.at_input_january, self.ui.at_slider_january),
            (self.ui.at_input_february, self.ui.at_slider_february),
            (self.ui.at_input_march, self.ui.at_slider_march),
            (self.ui.at_input_april, self.ui.at_slider_april),
            (self.ui.at_input_may, self.ui.at_slider_may),
            (self.ui.at_input_june, self.ui.at_slider_june),
            (self.ui.at_input_july, self.ui.at_slider_july),
            (self.ui.at_input_august, self.ui.at_slider_august),
        ]

        self.global_radiation_inputs = [
            (self.ui.gr_input_october, self.ui.gr_slider_october),
            (self.ui.gr_input_november, self.ui.gr_slider_november),
            (self.ui.gr_input_december, self.ui.gr_slider_december),
            (self.ui.gr_input_january, self.ui.gr_slider_january),
            (self.ui.gr_input_february, self.ui.gr_slider_february),
            (self.ui.gr_input_march, self.ui.gr_slider_march),
            (self.ui.gr_input_april, self.ui.gr_slider_april),
            (self.ui.gr_input_may, self.ui.gr_slider_may),
            (self.ui.gr_input_june, self.ui.gr_slider_june),
            (self.ui.gr_input_july, self.ui.gr_slider_july),
            (self.ui.gr_input_august, self.ui.gr_slider_august),
        ]

        self.precipitation_inputs = [
            (self.ui.p_input_october, self.ui.p_slider_october),
            (self.ui.p_input_november, self.ui.p_slider_november),
            (self.ui.p_input_december, self.ui.p_slider_december),
            (self.ui.p_input_january, self.ui.p_slider_january),
            (self.ui.p_input_february, self.ui.p_slider_february),
            (self.ui.p_input_march, self.ui.p_slider_march),
            (self.ui.p_input_april, self.ui.p_slider_april),
            (self.ui.p_input_may, self.ui.p_slider_may),
            (self.ui.p_input_june, self.ui.p_slider_june),
            (self.ui.p_input_july, self.ui.p_slider_july),
            (self.ui.p_input_august, self.ui.p_slider_august),
        ]

        self.humidity_inputs = [
            (self.ui.h_input_october, self.ui.h_slider_october),
            (self.ui.h_input_november, self.ui.h_slider_november),
            (self.ui.h_input_december, self.ui.h_slider_december),
            (self.ui.h_input_january, self.ui.h_slider_january),
            (self.ui.h_input_february, self.ui.h_slider_february),
            (self.ui.h_input_march, self.ui.h_slider_march),
            (self.ui.h_input_april, self.ui.h_slider_april),
            (self.ui.h_input_may, self.ui.h_slider_may),
            (self.ui.h_input_june, self.ui.h_slider_june),
            (self.ui.h_input_july, self.ui.h_slider_july),
            (self.ui.h_input_august, self.ui.h_slider_august),
        ]

    def __selectFile__(
        self,
        title,
        path,
        filetype,
    ):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(
            self,
            title,
            path,
            '{};;All Files (*)'.format(
                filetype,
            ),
            options=options
        )

        if fileName:
            return fileName
        else:
            return None

    def __importCsv__(
        self,
        csv_file,
        csv_separator=';',
    ):
        df = pd.read_csv(
            csv_file,
            sep=csv_separator,
            index_col=0,
        )

        df.reset_index(
            drop=True,
            inplace=True,
        )

        return df

    def __selectYear__(
        self,
        year,
    ):
        self.ui.button_select_weather_season_file.setEnabled(True)

        self.selected_year = year.text()

        year_begin = '{}-01-01'.format(self.selected_year)
        year_end = '{}-12-31'.format(self.selected_year)

        self.year_yield = self.crop_season.loc[
            (self.crop_season['harvesting_date'] >= year_begin) &
            (self.crop_season['harvesting_date'] <= year_end)
        ]['yield']

        self.year_yield = self.year_yield.values[0]
        year_yield_formatted = str(self.year_yield)

        self.ui.output_year_yield.setText(year_yield_formatted)

    def __enableInputs__(
        self,
        input_tuples,
        values,
    ):
        slider_max = max(values) * 1000 * 2
        slider_min = -slider_max

        for index in range(len(values)):
                value = round(values[index], 2)

                input_tuple = input_tuples[index]

                text = input_tuple[0]
                slider = input_tuple[1]

                text.setText(str(value))

                slider.setMinimum(slider_min)
                slider.setMaximum(slider_max)
                slider.setValue(value * 1000)
                slider.setEnabled(True)

                self.__linkSliderText__(slider, text)

    def __linkSliderText__(
        self,
        slider,
        text,
    ):
        slider.valueChanged.connect(lambda x: [
            text.setText(
                str(round(x / 1000, 2))
            ),
            self.predict()
        ])

    def __getValuesFromInputs__(
        self,
        input_tuples,
    ):
        values = np.empty((len(input_tuples)))

        for index in range(len(input_tuples)):
            input_tuple = input_tuples[index]

            text = input_tuple[0]

            values[index] = float(text.text())

        return values

    def selectCropSeasonFile(self):
        crop_season_file = self.__selectFile__(
            'Select crop season file',
            '../../data/models-data',
            'Crop season file (crop-seasons.csv)'
        )

        if crop_season_file is not None:
            splitted = crop_season_file.split('/')
            self.selected_crop = splitted[-2]

            self.crop_season = self.__importCsv__(crop_season_file)

            self.crop_season['sowing_date'] = pd.to_datetime(
                self.crop_season['sowing_date'],
                format='%Y-%m-%d',
            )

            self.crop_season['harvesting_date'] = pd.to_datetime(
                self.crop_season['harvesting_date'],
                format='%Y-%m-%d',
            )

            years = self.crop_season[
                'harvesting_date'
            ].dt.strftime('%Y').values

            self.ui.output_crop.setText(self.selected_crop)

            self.ui.list_years.clear()

            for year in years:
                item = QListWidgetItem(year)

                self.ui.list_years.addItem(item)

            self.ui.list_years.itemClicked.connect(
                self.__selectYear__,
            )

            self.ui.list_years.setEnabled(True)

    def selectWeatherSeasonFile(self):
        weather_season_file = self.__selectFile__(
            'Select weather season file',
            '../../data/models-data/{}/by-M'.format(
                self.selected_crop,
            ),
            'Weather season file (resampled-weather-data-year-{}.csv)'.format(
                self.selected_year,
            )
        )

        self.weather_season = self.__importCsv__(weather_season_file)

        self.ui.button_select_model_file.setEnabled(True)

    def selectModelFile(self):
        model_file = self.__selectFile__(
            'Select model file',
            '../../data/models/{}'.format(
                self.selected_crop,
            ),
            (
                'Model season file ('
                '*-neurons-*-epochs-*-loss-function-*-optimizer-'
                'weather-resampled-by-M-*-fn-*-scaler.hdf5'
                ')'
            )
        )

        self.model = load_model(model_file)

        splitted = model_file.split('/')
        splitted = splitted[-1]
        splitted = splitted.split('-')

        neurons_position = splitted.index('neurons')
        epochs_position = splitted.index('epochs')
        loss_position = splitted.index('loss')
        optimizer_position = splitted.index('optimizer')
        fn_position = splitted.index('fn')
        scaler_position = splitted.index('scaler.hdf5')

        self.neurons = splitted[0:neurons_position][0]
        self.epochs = splitted[neurons_position + 1:epochs_position][0]
        self.loss_function = '_'.join(
            splitted[epochs_position + 1:loss_position]
        )
        self.optimizer = splitted[optimizer_position - 1:optimizer_position][0]
        self.weather_resample_fn = splitted[fn_position - 1:fn_position][0]
        self.weather_scaler = splitted[fn_position + 1:scaler_position][0]

        self.ui.output_model_details.insertPlainText((
            'Neurons:\n{}\n\n'
            'Epochs:\n{}\n\n'
            'Loss function:\n{}\n\n'
            'Optimizer:\n{}\n\n'
            'Weather resample function:\n{}\n\n'
            'Weather scaler:\n{}\n\n'

        ).format(
            self.neurons,
            self.epochs,
            self.loss_function,
            self.optimizer,
            self.weather_resample_fn,
            self.weather_scaler,
        ))

        if 'air_temperature' in self.weather_season.columns:
            values = self.weather_season['air_temperature'].values

            self.__enableInputs__(
                self.air_temperature_inputs,
                values,
            )

        if 'radiation' in self.weather_season.columns:
            values = self.weather_season['radiation'].values

            self.__enableInputs__(
                self.global_radiation_inputs,
                values,
            )

        if 'precipitation' in self.weather_season.columns:
            values = self.weather_season['precipitation'].values

            self.__enableInputs__(
                self.precipitation_inputs,
                values,
            )

        if 'humidity' in self.weather_season.columns:
            values = self.weather_season['humidity'].values

            self.__enableInputs__(
                self.humidity_inputs,
                values,
            )

        self.predict()

    def predict(self):
        number_of_weather_variables = len(
            self.weather_season.columns
        )

        weather_variables = np.empty((number_of_weather_variables, 11))

        index = 0
        if 'air_temperature' in self.weather_season.columns:
            values = self.__getValuesFromInputs__(
                self.air_temperature_inputs
            )

            weather_variables[index] = values
            index += 1

        if 'radiation' in self.weather_season.columns:
            values = self.__getValuesFromInputs__(
                self.global_radiation_inputs
            )

            weather_variables[index] = values
            index += 1

        if 'precipitation' in self.weather_season.columns:
            values = self.__getValuesFromInputs__(
                self.precipitation_inputs
            )

            weather_variables[index] = values
            index += 1

        if 'humidity' in self.weather_season.columns:
            values = self.__getValuesFromInputs__(
                self.humidity_inputs
            )

            weather_variables[index] = values
            index += 1

        weather_variables = pd.DataFrame(weather_variables).T

        scaler_fn = None

        if (self.weather_scaler == 'minmax'):
            scaler_fn = MinMaxScaler(feature_range=(-1, 1))
        elif (self.weather_scaler == 'standard'):
            scaler_fn = StandardScaler()

        weather_variables_scaled = pd.DataFrame(
            scaler_fn.fit_transform(weather_variables),
            columns=weather_variables.columns,
        )

        weather_variables_scaled = np.array(
            [weather_variables_scaled.values]
        )

        prediction = self.model.predict(weather_variables_scaled)
        prediction = prediction[0][0]
        prediction = round(prediction, 2)

        difference = abs(self.year_yield - prediction)
        average = (self.year_yield + prediction) / 2
        error_percentage = difference / average * 100
        error_percentage = round(error_percentage, 2)

        prediction = str(prediction)
        error_percentage = str(error_percentage)

        self.ui.output_prediction.setText(prediction)
        self.ui.output_error_percentage.setText(error_percentage)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = EnvironmentViewerAndPredictor()
    window.setWindowTitle('Smart Crops - Environment viewer and predictor')
    window.show()
    sys.exit(app.exec_())
